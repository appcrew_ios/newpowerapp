//
//  IntroViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 4..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit
import SQLite
import CoreData

class IntroViewController: UIViewController {
    //1. 빌드컴퓨터에서 소스 수정
    //1. 빌드컴퓨터에서 소스 두번째 수정
    var database: Database?
    var isFailGetShopInfo : Bool = true //상점정보를 처음 도메인으로 못가져 오면 true
    //2. 개발용컴퓨터에서 소스 수정
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        1. sqlite 호출하기
        database = Database()
        database?.createDB()
//        database?.insertDB()
        
//        2. powerapp
        switch UIApplication.shared.statusBarOrientation {
        case .landscapeLeft :
            view.transform = CGAffineTransform(rotationAngle: CGFloat(90 * Double(Double.pi/180)))
            break
        case .landscapeRight :
            view.transform = CGAffineTransform(rotationAngle: CGFloat(-90 * Double(Double.pi/180)))
            break
        default:
            break
        }
        
        //url을 타고 들어왔을경우 값넣어주고 초기화
        if TempVO.shared.inflow_id != "" {
            UserDefaults.standard.set(TempVO.shared.inflow_id, forKey: .tranceFlowId)
            UserDefaults.standard.set(TempVO.shared.inflow_date, forKey: .tranceFlowDate)
            TempVO.shared.inflow_id = ""
        }
        
        //앱이 죽어있을때 푸시를 클릭했을때 값을 넣어주고 초기화
        if TempVO.shared.notification_key != "" {
            UserDefaults.standard.set(TempVO.shared.inflow_id, forKey: .tranceNotiKey)
            UserDefaults.standard.set(TempVO.shared.inflow_date, forKey: .tranceNotiDate)
            TempVO.shared.notification_key = ""
        }
        
        if let timeNoti = UserDefaults.standard.string(forKey: .timeNoti) {
//            DFT_TRACE_PRINT(output: "timeNoti: \(timeNoti)")
        } else {
            let currentDate = getNowTimeString()
            UserDefaults.standard.set(currentDate, forKey: .timeNoti)
        }
        
//        2. 상점정보 가져오기
        getShopInfo()
        
//        3. 상점id로 상점url가져오기
//        shopInfoByShopID()
        
//        4. 공통alert
        
//        5. coredataInsert
//        coreDataInsert()
        
//        6.getCoreData 가져오기
//        getCoreData()
        
//        7.getSqllite()
//        getSqllite()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        getCustomAlertView()
//        checkImageUpdate()
        

    }
    
    func getSqllite() {
        database?.selectDB()
    }
    
    func getCoreData() {
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "NotificationEntity")
        request.returnsObjectsAsFaults = false
        let results = try! context.fetch(request) as NSArray
        if results.count > 0 {
            for res_obj in results {
                let res = res_obj as AnyObject
                database?.insertDB(data: res)
            }
            deleteCoredata()
        }
        
    }
    
    func deleteCoredata() {
        //getting context from your Core Data Manager Class
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let managedContext: NSManagedObjectContext = appDel.managedObjectContext
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NotificationEntity")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            print ("There is an error in deleting records")
        }
    }
    
    //coredata에 데이터 setup
    func coreDataInsert() {
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext
        let newNotice = NSEntityDescription.insertNewObject(forEntityName: "NotificationEntity",
                                                            into: context) as! NotificationEntity
        
        let date_str = "2018-11-06 10:10:10"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let push_date = dateFormatter.date(from: date_str)!
        
        
        newNotice.setValue("1", forKey: "key")
        newNotice.setValue(push_date, forKey: "date")
        newNotice.setValue("테스트테스트입니다.", forKey: "content")
        newNotice.setValue(1, forKey: "confirm")
        newNotice.setValue("test.html", forKey: "directlink")
        newNotice.setValue("testtype", forKey: "type")
        try! context.save()
    }
    
    func getCustomAlertView() {
        let alert = AlertView(title: nil, message: "testtest", okTitle: "확인", cancelTitle: "취소", okCompletion: {
            self.openOutSide(string: Config.shared.domain)
            exit(0)
        }, cancelCompletion: {
            exit(0)
        })
        self.present(alert, animated: true, completion: nil)
    }
    
    func checkImageUpdate() {
        defer {//마지막에 로그인체크 진행
            checkLogin()
        }
        //인트로 이미지는 main쪽에서 보여주는걸로 수정됨 gif를 넣어야 하기 때문에
    }
    
    func checkLogin() {
        let now = Date() // 현재 날짜 및 시간 체크
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ko_KR") // 로케일 설정
        dateFormatter.dateFormat = "yyyyMMdd" // 날짜 형식 설정
        let dateNow = Int(dateFormatter.string(from: now))!
        let arrExpire = Config.shared.enddate.components(separatedBy: "-")
        let dateExpire = Int(String("\(arrExpire[0])\(arrExpire[1])\(arrExpire[2])"))!
        
        let compareVer : String = Config.shared.ios_version
        let currentVer : String = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String// 1.0.0
        
        TempVO.shared.localVersion = currentVer
        
        
        guard dateExpire >= dateNow else {
            let alert = AlertView(title: nil, message: ERROR_END_USEDATE, okTitle: "확인", cancelTitle: "취소", okStyle: .default, okCompletion: {
                self.openOutSide(string: Config.shared.domain)
                exit(0)
            }, cancelCompletion: {
                exit(0)
            })
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        
        itunesLookup(tryGetAppleID: true) {
            var appleID = ""
            if let id = UserDefaults.standard.string(forKey: .appleID) {
                appleID = id
            }
            
            if isNewestVersionApp(local: currentVer, server: compareVer) {
//                DFT_TRACE_PRINT(output:"현재 버전이 같거나 최신버전입니다 currentVer = \(currentVer) , compareVer = \(compareVer)")
                //탈옥체크
//                self.checkBroken()
                
                //루트뷰 변경
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
                
                //메인뷰로 이동
                //기존 루트뷰 였던 인트로VC에서 메인VC로 루트뷰 변경
                UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: viewController)
                viewController.navigationController?.isNavigationBarHidden = true
            } else {
                DFT_TRACE_PRINT(output:"현재 버전이 구버전입니다 currentVer = \(currentVer) , compareVer = \(compareVer)")
                let alert = AlertView(title: nil, message: ERROR_UPDATE, okTitle: "확인", okStyle: .default, okCompletion: {
                    if appleID == "" {
                        self.openOutSide(string:"itms-apps://itunes.apple.com/")
                    } else {
                        self.openOutSide(string:"itms-apps://itunes.apple.com/app/id\(appleID)")
                    }
                    exit(0)
                }, cancelCompletion: {
                    exit(0)
                })
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
    }
    
    ///외부로 열기
    func openOutSide(string: String) {
        guard let url = URL(string: string) else {
            return
        }
        if let urlString = URL(string: "\(url)") {
            UIApplication.shared.open(urlString, options: [:], completionHandler: nil)
        } else {
            return
        }
    }
    
    ///커스텀메뉴가 바로톡일 경우 기본 이미지 경로 변경
    func isBaroTalkMenu(name custom_name: String ,botMenu bm: BotMenu) -> String {
        guard custom_name == "barotalk"  else {
            return bm.basicImageName
        }
        let basicImageName = "menu_baro"
        return basicImageName
    }
    
    func checkBroken() { //탈옥 체크
        let filePath = "/Applications/Cydia.app"
        var cydiaIs = false
        var bashIs = false
        if FileManager.default.fileExists(atPath: filePath) {
            cydiaIs = true
        }
        
        let bashPath = "/bin/bash"
        if FileManager.default.fileExists(atPath: bashPath) {
            let f = fopen(bashPath, "r")
            if f != nil {
                defer { fclose(f) }
                bashIs = true
            }
        }
        
        if cydiaIs || bashIs {
            let alertBroken = UIAlertController(title: "", message: "앱 무결성 체크중입니다.\n탈옥폰인 경우 앱 이용이 제한될 수 있습니다.", preferredStyle: .alert)
            let cancelAtion = UIAlertAction(title: "확인", style: .cancel) { (_) in
                if let urlString = URL(string: Config.shared.domain+"/m/app.html") {
                    UIApplication.shared.open(urlString, options: [:], completionHandler: nil)
                }
                exit(0)
            }
            alertBroken.addAction(cancelAtion)
            self.present(alertBroken, animated: true, completion: nil)
            return
        }
    }
    
    func getShopInfo(){
        let conf = Config.shared
        
        let parameters_shopinfo = [
            "admin_id": conf.shopID,
            "type": "get_Setting_data",
            "user_id": "OTk5OQ=="
        ]
        let shopinfo_url = "\(conf.domain)/list/API/powerapp_info.html"
        DataDispatch().getJsonData(url: shopinfo_url, method: POST, parm: parameters_shopinfo,
            success: { (response)-> Void in
                guard let jsonResult = response as? NSDictionary else {
                    return
                }
//                print("jsonResult: \(jsonResult)")
                if let result = jsonResult.value(forKey: "result") as? Bool, result {
                    let shopData = jsonResult.value(forKey: "data") as! NSDictionary
                    //하단메뉴 체크를 위해서 data저장
                    
                    conf.auth_type = shopData.value(forKey: "auth_type") as? String ?? ""
                    conf.back_path = shopData.value(forKey: "back_path") as? String ?? ""
                    conf.basket_path = shopData.value(forKey: "basket_path") as? String ?? ""
                    conf.category_path = shopData.value(forKey: "category_path") as? String ?? ""
                    conf.delivery_path = shopData.value(forKey: "delivery_path") as? String ?? ""
                    conf.forward_path = shopData.value(forKey: "forward_path") as? String ?? ""
                    conf.home_path = shopData.value(forKey: "home_path") as? String ?? ""
                    conf.invite_path = shopData.value(forKey: "invite_path") as? String ?? ""
                    conf.push_path = shopData.value(forKey: "push_path") as? String ?? ""
                    conf.menu_path = shopData.value(forKey: "menu_path") as? String ?? ""
                    conf.mypage_path = shopData.value(forKey: "mypage_path") as? String ?? ""
                    conf.review_path = shopData.value(forKey: "review_path") as? String ?? ""
                    conf.refresh_path = shopData.value(forKey: "refresh_path") as? String ?? ""
                    
                    conf.scrap_path = shopData.value(forKey: "scrap_path") as? String ?? ""
                    
                    conf.custom_menu_1_path = shopData.value(forKey: "custom_menu_1_path") as? String ?? ""
                    conf.custom_menu_2_path = shopData.value(forKey: "custom_menu_2_path") as? String ?? ""
                    conf.custom_menu_3_path = shopData.value(forKey: "custom_menu_3_path") as? String ?? ""
                    conf.custom_menu_4_path = shopData.value(forKey: "custom_menu_4_path") as? String ?? ""
                    conf.custom_menu_5_path = shopData.value(forKey: "custom_menu_5_path") as? String ?? ""
                    conf.custom_menu_6_path = shopData.value(forKey: "custom_menu_6_path") as? String ?? ""
                    conf.custom_menu_7_path = shopData.value(forKey: "custom_menu_7_path") as? String ?? ""
                    conf.custom_menu_8_path = shopData.value(forKey: "custom_menu_8_path") as? String ?? ""
                    conf.custom_menu_9_path = shopData.value(forKey: "custom_menu_9_path") as? String ?? ""
                    conf.custom_menu_10_path = shopData.value(forKey: "custom_menu_10_path") as? String ?? ""
                    
                    conf.enddate = shopData.value(forKey: "enddate") as? String ?? ""
                    conf.intro_android = shopData.value(forKey: "intro_android") as? String ?? ""
                    conf.intro_ios = shopData.value(forKey: "intro_ios") as? String ?? ""
                    
                    conf.invitation_benefit = shopData.value(forKey: "invitation_benefit") as? String ?? ""
                    conf.is_invitation = shopData.value(forKey: "is_invitation") as? Bool ?? false
                    
                    conf.is_join = shopData.value(forKey: "is_join") as? Bool ?? false
                    
                    conf.menu_color = shopData.value(forKey: "menu_color") as? String ?? ""
                    conf.password_rule = shopData.value(forKey: "password_rule") as? String ?? ""
                    conf.popup_image = shopData.value(forKey: "popup_image") as? String ?? ""
                    conf.popup_link = shopData.value(forKey: "popup_link") as? String ?? ""
                    conf.push_benefit = shopData.value(forKey: "push_benefit") as? String ?? ""
                    conf.push_color = shopData.value(forKey: "push_color") as? String ?? ""
                    conf.push_receive_day = shopData.value(forKey: "push_receive_day") as? String ?? ""
                    conf.server = shopData.value(forKey: "server") as? String ?? ""
                    conf.shopname = shopData.value(forKey: "shopname") as? String ?? ""
                    conf.update_date = shopData.value(forKey: "update_date") as? String ?? ""
                    conf.use_barcode = shopData.value(forKey: "use_barcode") as? String ?? ""
                    
                    conf.use_back_btn = shopData.value(forKey: "use_back_btn") as? Bool ?? false
                    conf.use_basket_btn = shopData.value(forKey: "use_basket_btn") as? Bool ?? false
                    conf.use_category_btn = shopData.value(forKey: "use_category_btn") as? Bool ?? false
                    conf.use_delivery_btn = shopData.value(forKey: "use_delivery_btn") as? Bool ?? false
                    conf.use_forward_btn = shopData.value(forKey: "use_forward_btn") as? Bool ?? false
                    conf.use_home_btn = shopData.value(forKey: "use_home_btn") as? Bool ?? false
                    
                    conf.use_invitation = shopData.value(forKey: "use_invitation") as? String ?? ""
                    conf.use_invite_btn = shopData.value(forKey: "use_invite_btn") as? Bool ?? false
                    
                    conf.use_menu_btn = shopData.value(forKey: "use_menu_btn") as? Bool ?? false
                    conf.use_mypage_btn = shopData.value(forKey: "use_mypage_btn") as? Bool ?? false
                    
                    conf.use_popup = shopData.value(forKey: "use_popup") as? String ?? ""
                    
                    conf.use_join = shopData.value(forKey: "use_join") as? Bool ?? false
                    
                    conf.use_push_btn = shopData.value(forKey: "use_push_btn") as? Bool ?? false
                    conf.use_review_btn = shopData.value(forKey: "use_review_btn") as? Bool ??  false
                    
                    conf.use_simple_review = shopData.value(forKey: "use_simple_review") as? String ?? ""
                    conf.use_simple_join = shopData.value(forKey: "use_simple_join") as? String ?? ""
                    
                    conf.ios_version = shopData.value(forKey: "ios_version") as? String ?? ""
                    conf.free_shop = shopData.value(forKey: "free_shop") as? Bool ?? false
                    
                    conf.push_agree = shopData.value(forKey: "push_agree") as? String ?? ""
                    
                    conf.use_offline_push = shopData.value(forKey: "use_offline_push") as? Bool ?? false
                    
                    
                    conf.sns_facebook = shopData.value(forKey: "sns_facebook") as? String ?? "N"
                    conf.sns_kakao = shopData.value(forKey: "sns_kakao") as? String ?? "N"
                    conf.sns_naver = shopData.value(forKey: "sns_naver") as? String ?? "N"
                    
                    conf.bottom_display = shopData.value(forKey: "bottom_display") as? String ?? "Y"
                    
                    conf.use_refresh_btn = shopData.value(forKey: "use_refresh_btn") as? Bool ?? false
                    //url 퍼가기 사용 유무
                    conf.use_scrap_btn = shopData.value(forKey: "use_scrap_btn") as? Bool ?? false
                    //커스텀버튼 사용 유무
                    conf.use_custom_menu_1_btn = shopData.value(forKey: "use_custom_menu_1_btn") as? Bool ?? false
                    conf.use_custom_menu_2_btn = shopData.value(forKey: "use_custom_menu_2_btn") as? Bool ?? false
                    conf.use_custom_menu_3_btn = shopData.value(forKey: "use_custom_menu_3_btn") as? Bool ?? false
                    conf.use_custom_menu_4_btn = shopData.value(forKey: "use_custom_menu_4_btn") as? Bool ?? false
                    conf.use_custom_menu_5_btn = shopData.value(forKey: "use_custom_menu_5_btn") as? Bool ?? false
                    conf.use_custom_menu_6_btn = shopData.value(forKey: "use_custom_menu_6_btn") as? Bool ?? false
                    conf.use_custom_menu_7_btn = shopData.value(forKey: "use_custom_menu_7_btn") as? Bool ?? false
                    conf.use_custom_menu_8_btn = shopData.value(forKey: "use_custom_menu_8_btn") as? Bool ?? false
                    conf.use_custom_menu_9_btn = shopData.value(forKey: "use_custom_menu_9_btn") as? Bool ?? false
                    conf.use_custom_menu_10_btn = shopData.value(forKey: "use_custom_menu_10_btn") as? Bool ?? false
                    
                    //커스텀버튼 내용
                    if let menuBtn_obj = shopData.value(forKey: "use_custom_menu_btn") as AnyObject? {
                        if conf.use_custom_menu_1_btn {
                            conf.custom_1_link = menuBtn_obj.value(forKey: "custom_1_link") as? String ?? ""
                            conf.custom_1_name = menuBtn_obj.value(forKey: "custom_1_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_2_btn {
                            conf.custom_2_link = menuBtn_obj.value(forKey: "custom_2_link") as? String ?? ""
                            conf.custom_2_name = menuBtn_obj.value(forKey: "custom_2_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_3_btn {
                            conf.custom_3_link = menuBtn_obj.value(forKey: "custom_3_link") as? String ?? ""
                            conf.custom_3_name = menuBtn_obj.value(forKey: "custom_3_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_4_btn {
                            conf.custom_4_link = menuBtn_obj.value(forKey: "custom_4_link") as? String ?? ""
                            conf.custom_4_name = menuBtn_obj.value(forKey: "custom_4_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_5_btn {
                            conf.custom_5_link = menuBtn_obj.value(forKey: "custom_5_link") as? String ?? ""
                            conf.custom_5_name = menuBtn_obj.value(forKey: "custom_5_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_6_btn {
                            conf.custom_6_link = menuBtn_obj.value(forKey: "custom_6_link") as? String ?? ""
                            conf.custom_6_name = menuBtn_obj.value(forKey: "custom_6_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_7_btn {
                            conf.custom_7_link = menuBtn_obj.value(forKey: "custom_7_link") as? String ?? ""
                            conf.custom_7_name = menuBtn_obj.value(forKey: "custom_7_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_8_btn {
                            conf.custom_8_link = menuBtn_obj.value(forKey: "custom_8_link") as? String ?? ""
                            conf.custom_8_name = menuBtn_obj.value(forKey: "custom_8_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_9_btn {
                            conf.custom_9_link = menuBtn_obj.value(forKey: "custom_9_link") as? String ?? ""
                            conf.custom_9_name = menuBtn_obj.value(forKey: "custom_9_name") as? String ?? ""
                        }
                        
                        if conf.use_custom_menu_10_btn {
                            conf.custom_10_link = menuBtn_obj.value(forKey: "custom_10_link") as? String ?? ""
                            conf.custom_10_name = menuBtn_obj.value(forKey: "custom_10_name") as? String ?? ""
                        }
                    }
                    
                    
                    //알림내용보기 SNS공유 값
                    conf.kakao_js_key = shopData.value(forKey: "kakao_js_key") as? String ?? ""
                    
                    conf.use_share_fb = shopData.value(forKey: "use_share_fb") as? Bool ?? false
                    conf.use_share_tw = shopData.value(forKey: "use_share_tw") as? Bool ?? false
                    conf.use_share_ka = shopData.value(forKey: "use_share_ka") as? Bool ?? false
                    conf.use_share_ks = shopData.value(forKey: "use_share_ks") as? Bool ?? false
                    
                    // 키워드 알림 실행 여부 값
                    conf.is_keyword = shopData.value(forKey: "use_keyword") as? Bool ?? false
                    
                    //슬라이드 메뉴 GUI 변경 값
                    conf.slide_color = shopData.value(forKey: "slide_color") as? String ?? "DEFAULT"
                    
                    conf.menu_user_info = shopData.value(forKey: "menu_user_info") as? String ?? "N"
                    
                    conf.slide_type = shopData.value(forKey: "slide_type") as? String ?? "HALF"
                    
                    //MARK: - 슬라이드 메뉴 타이틀 GUI 변경 값
                    if let slide_T_info = shopData.value(forKey: "slide_T_info") as? NSDictionary {
                        conf.slide_T_color = slide_T_info.value(forKey: "color") as? String ?? conf.slide_color
                        conf.slide_T_align = slide_T_info.value(forKey: "align") as? String ?? "CENTER"
                    }
                    
                    // B2B상점인지 성인몰 인지 구분하기 위함
                    conf.adult_type = shopData.value(forKey: "adult_type") as? String ?? "N"
                    
                    conf.intro_image_array = shopData.value(forKey: "use_intro_image") as? [String] ?? [String]()
                    
                    conf.use_intro_time = shopData.value(forKey: "use_intro_time") as? String ?? "2"
                    
                    conf.use_intro_effect = shopData.value(forKey: "use_intro_effect") as? String ?? "DISAPPEAR"
                    
                    conf.use_keyword = shopData.value(forKey: "use_keyword") as? Bool ?? false
                    
                    // SDK log domain 값
                    if let log_url = shopData.value(forKey: "log_url") as? String {
//                        TrackingAgent.logDomain(log_url)
                    }
                    // log domain값 강제 적용 상점 11개, 테스트용 1개
                    let loapp1ShopList : [String] = [
                        "yamiyami",
                        "aboki",
                        "label55",
                        "compactd",
                        "jason006",
                        "yeoek",
                        "superstari",
                        "tommyboy79",
                        "ssodo82",
                        "aden08",
                        "jogunshop",
                        "designtest77"
                    ]
                    for shop in loapp1ShopList {
                        if conf.shopID == shop {
//                            TrackingAgent.logDomain("logapp1")
                        }
                    }
                    
                    // SDK shop_category 값
                    if let shop_category = shopData.value(forKey: "shop_category") as? String, shop_category != "" {
                        Config.shared.shop_category = shop_category
//                        PowerappSDK.TrackingAgent.setCategory(shop_category)
                    }
                    
                    // 메뉴 순서 체크
                    TempVO.shared.botMenus = [BotMenu]()
                    
                    for (i, menuName) in TempVO.botMenuArr.enumerated() {
                        
                        //상점정보에 메뉴가 없으면 넘어감
                        guard TempVO.shared.shopInfoData_str.contains(menuName) else { continue }
                        
                        let bm = BotMenu(type: menuName, tag: i)
                        
                        for (menu, file, basicImage, btnPath, useBtn) in TempVO.shared.botMenuTuples {
                            if menuName == menu {
                                bm.fileName = file
                                bm.basicImageName = basicImage
                                bm.path = btnPath
                                
                                if useBtn {
                                    //메뉴 순서를 입력하기 위한 전체 데이터에 해당 메뉴 위치값
                                    bm.method = String(TempVO.shared.shopInfoData_str.components(separatedBy: "\"\(menuName)")[0]).count
                                    
                                    TempVO.shared.botMenus.append(bm)
                                }
                            }
                        }
                        
                        //커스텀메뉴
                        for (menu, file, basicImage, btnPath, useBtn, name) in TempVO.shared.botCustomMenuTuples {
                            if menuName == menu {
                                bm.fileName = file
                                bm.basicImageName = basicImage
                                bm.path = btnPath
                                
                                if useBtn && !name.isEmpty {
                                    bm.basicImageName = self.isBaroTalkMenu(name: name, botMenu: bm)
                                    //메뉴 순서를 입력하기 위한 전체 데이터에 해당 메뉴 위치값
                                    bm.method = String(TempVO.shared.shopInfoData_str.components(separatedBy: "\"\(menuName)")[0]).count
                                    TempVO.shared.botMenus.append(bm)
                                }
                            }
                        }
                    }
                    
                    // 메뉴 길이로 순서 소팅
                    TempVO.shared.botMenus.sort(by: { $0.method < $1.method })
                    // 메뉴 갯수 체크
                    conf.menu_num = TempVO.shared.botMenus.count
                    // 무료샵일 경우 메뉴 고정
                    //                            if conf.free_shop {
                    //                                //conf.menu_num = 7
                    //                                conf.menu_num = 6
                    //                            }
                    
                    // 매장 푸시는 다른 프로젝트에서 작업
                    DFT_TRACE_PRINT(output: "conf.use_offline_push: \(conf.use_offline_push), sns_facebook: \(conf.sns_facebook), sns_kakao: \(conf.sns_kakao), sns_naver: \(conf.sns_naver)")
                    
                    if !self.isFailGetShopInfo {
                        //상점정보 가져오는 것에 한번 실패하여 상점아이디로 도메인을 가져와 상점정보를 호출하였을때 호출에 성공하면 상점 URL을 저장시킨다.
                        UserDefaults.standard.set(Config.shared.shopURL, forKey: .shopInfoByShopID)
                    }
                    
                    self.checkImageUpdate()
                } else {
                    if self.isFailGetShopInfo {
                        //상점 정보를 도메인으로 못 가져오면 상점 아이디로 상점 도메인을 가져와서 다시 상점 정보를 가져오도록 한다.
                        self.shopInfoByShopID()
                    } else {
                        if let msg = jsonResult["msg"] as? String, !msg.isEmpty {
                            let content = msg.replacingOccurrences(of: "+", with: " ",
                                                                   options: NSString.CompareOptions.literal, range: nil)
                                .removingPercentEncoding!
                                .replacingOccurrences(of: "\\n", with: "\n",
                                                      options: NSString.CompareOptions.literal, range: nil)
                            let alert = AlertView(title: nil, message: content, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
                                exit(0)
                            }, cancelCompletion: nil)
                            self.present(alert, animated: true, completion: nil)
                            DFT_TRACE_PRINT(output: "msg: \(content)")
                        }
                    }
                }
        },  fail: { (response)->Void in
            if self.isFailGetShopInfo {
                //상점 정보를 도메인으로 못 가져오면 상점 아이디로 상점 도메인을 가져와서 다시 상점 정보를 가져오도록 한다.
                self.shopInfoByShopID()
            } else {//상점 아이디로 가져온 도메인에서도 상점정보를 못 받으면 앱을 종료 시킨다.
                DFT_TRACE_PRINT(output:"\(Config.shared.shopID) domain get info error: \(response)")
                let alert = AlertView(title: nil, message: ERROR_TIMEOUT, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
                    exit(0)
                }, cancelCompletion: nil)
                self.present(alert, animated: true, completion: nil)
            }
            return
        })
    }
    
    //상점정보 불러오기 실패했을때 상점 아이디로 도메인을 다시 가져온다.
    func shopInfoByShopID() {
        isFailGetShopInfo = false
        
        let parameters_shopID = [
            "id": Config.shared.shopID
        ]
        let shopID_url = "http://makeshop-api.makeshop.co.kr/appinfo/list.html"
        DataDispatch().getStringData(url: shopID_url, method: POST, parm: parameters_shopID,
            success: { (response)-> Void in
                let newShopURL = response
                if newShopURL.hasPrefix("http://") {
                    let newShopURL2 = newShopURL.replacingOccurrences(of: "http://", with: "",
                                                                      options: NSString.CompareOptions.literal, range: nil)
                    Config.shared.shopURL = newShopURL2
                } else if newShopURL.hasPrefix("https://") {
                    let newShopURL3 = newShopURL.replacingOccurrences(of: "http://", with: "",
                                                                      options: NSString.CompareOptions.literal, range: nil)
                    Config.shared.shopURL = newShopURL3
                } else {
                    Config.shared.shopURL = newShopURL //"designtest77.makemall.kr"
                }
                Config.shared.introImage_url = "\(Config.shared.domain)/shopimages/\(Config.shared.shopID)/powerapp_intro_android.png"
                
                DFT_TRACE_PRINT(output: "\(Config.shared.shopURL) Config.shared.shopURL = \(response) , shop domain = \(Config.shared.domain) error str = \(Config.shared.error_str)")
                self.getShopInfo()
        }, fail: { (response)->Void in
            let alert = AlertView(title: nil, message: ERROR_TIMEOUT, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
                exit(0)
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
            
        })
    }
    
}
