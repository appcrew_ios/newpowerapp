//
//  AES256Util.h
//  PowerApp
//
//  Created by Shawn Chun on 2015. 1. 27..
//  Copyright (c) 2015년 Koeracenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AES256Util : NSObject

+(NSString*)encWithString:(NSString*)string key:(NSString*)key;
+(NSString*)decWithString:(NSString*)string key:(NSString*)key;

@end
