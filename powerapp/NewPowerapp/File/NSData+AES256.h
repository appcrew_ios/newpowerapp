//
//  NSData+AES256.h
//  PowerApp
//
//  Created by Shawn Chun on 2015. 1. 27..
//  Copyright (c) 2015년 Koeracenter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (AES256)

- (NSData *)AES256EncryptWithKey: (NSString *)key;
- (NSData *)AES256DecryptWithKey: (NSString *)key;

@end
