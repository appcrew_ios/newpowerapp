//
//  ViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 8. 31..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        1. 상점리스트 가져오기
//        getShopInfo()
        
//        2. alertview공통 // viewdidappear에서 해줘야함
//        getCustomAlertView()
        
//        3. UserDefaults class
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //상점리스트 가져오기
    func getShopInfo(){
        let parameters_shopinfo = [
            "admin_id": "designtest77",
            "type": "get_Setting_data",
            "user_id": "OTk5OQ=="
        ]
        let shopinfo_url = "http://designtest77.makemall.kr/list/API/powerapp_info.html"
        DataDispatch().getJsonData(url: shopinfo_url, method: POST, parm: parameters_shopinfo,
                                   success: { (response)-> Void in
                                    guard let jsonResult = response as? NSDictionary else {
                                        return
                                    }
                                    
                                    if let result = jsonResult.value(forKey: "result") as? Bool, result {
                                        let shopData = jsonResult.value(forKey: "data") as! NSDictionary
                                        print(shopData)
                                    }
        },  fail: { (response)->Void in
            print("response: \(response)")
        })
    }
    
    //2 alertview공통
    func getCustomAlertView() {
        let alert = AlertView(title: nil, message: "testtest", okTitle: "확인", cancelTitle: "취소", okStyle: .destructive, okCompletion: {
            print("####################")
        }, cancelCompletion: nil)
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func clickSet(_ sender: Any) {
//        UserDefaults.standard.set("test1123", forKey: .test)
        
//        UserDefaults.standard.remove(forKey: .test)
    }
    @IBAction func clickGet(_ sender: Any) {
        if let token = UserDefaults.standard.string(forKey: .test) {
            print("token: \(token)")
        } else {
            print("token: null")
        }
    }
    
}

