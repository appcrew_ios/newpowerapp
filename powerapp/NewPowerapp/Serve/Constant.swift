//
//  Constant.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 3..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit

//Rootviewcontroller
let rootVC = UIApplication.shared.delegate?.window??.rootViewController

//screenSize
let screenSize = UIScreen.main.bounds

//Method string
let POST = "POST"
let GET = "GET"

//통신에러 string
let ERROR_NOT_CONNECT_INTERNET = "네트워크가 연결되지 않았습니다. 연결 후 서비스를 이용해주세요."
let ERROR_TIMEOUT = "현재 스마트폰 네트워크 상태가 좋지 않아\n상점 정보를 불러오지 못하고 있습니다.\n잠시 후 다시 이용해 주세요."
let ERROR_NOT_FIND_URL = "URL을 찾을 수 없습니다. \(Config.shared.shopname)에 문의해주세요."
let ERROR_ETC = "네트워크를 연결에 오류가 발생했습니다. \(Config.shared.shopname)에 문의해주세요."
let ERROR_NOT_METHOD = "전송 방식이 잘못됬습니다."

//ERROR string
let ERROR_END_USEDATE = "앱 사용기간이 만료되어 접속이 불가능 합니다.\n사파리로 접속 하시겠습니까?"
let ERROR_UPDATE = "최적화 된 환경에서 쇼핑할 수 있도록\n업데이트로 이동합니다."
let ERROR_MOVE_PAGE = "이동할 페이지가 없습니다."
let ERROR_PAGE = "해당 주소로 이동 할 수 없습니다."


//슬라이드메뉴 string
let SLIDE_DEFAULT = "DEFAULT"
let SLIDE_BLACK = "BLACK"
let SLIDE_WHITE = "WHITE"
let SLIDE_GRAY = "GRAY"
let SLIDE_T_CENTER = "CENTER"

