//
//  DataDispatch.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 8. 31..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit
import Alamofire

class DataDispatch {
    
    let indicator = AlertIndicator()
    
   /* shopinfo_url : http://designtest77.makemall.kr/list/API/powerapp_info.html, parameters_shopinfo :
    ["admin_id": "designtest77", "type": "get_Setting_data", "user_id": "OTk5OQ=="]
     */
    //url,method,parameters
    func getJsonData(url: String, method: String, parm: [String:String], success:@escaping (Any) -> Void, fail:@escaping (String) -> Void) {
        indicator.startAnimating()
        let methodStr = getMethod(method)
        Alamofire.request(url, method: methodStr, parameters: parm).responseJSON { response in
            //통신 에러일때 처리
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.indicator.stopAnimating()
            }
            if let error = response.error as NSError? {
                self.getErrorCode(error)
            }
            switch response.result {
            case .success(let resultData):
                if url.contains("powerapp_info.html") {
                    TempVO.shared.shopInfoData_str = String(data: response.data!, encoding: .utf8)!
                }
                success(resultData)
            case .failure(let resultData):
                fail(resultData.localizedDescription)
            }
        }
    }
    
    func getStringData(url: String, method: String, parm: [String:String], success:@escaping (String) -> Void, fail:@escaping (String) -> Void) {
        indicator.startAnimating()
        let methodStr = getMethod(method)
        Alamofire.request(url, method: methodStr, parameters: parm).responseString { response in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.indicator.stopAnimating()
            }
            if let error = response.error as NSError? {
                self.getErrorCode(error)
            }
            switch response.result {
            case .success(let resultData):
                success(resultData)                
            case .failure(let resultData):
                fail(resultData.localizedDescription)
            }
        }
    }
    
    //통신에러
    func getErrorCode(_ error: NSError) {
        let code = error.code
        var errorMessage = ""
        switch code {
        case -1001:
            //네트워크 상태가 불안정합니다. 네트워크 연결을 확인해주세요
            errorMessage = ERROR_TIMEOUT
            break
        case -1003:
            //URL을 찾을 수 없습니다. 문의해주세요.
            errorMessage = ERROR_NOT_FIND_URL
            break
        case -1009:
            //네트워크가 연결되지 않았습니다. 연결 후 서비스를 이용해주세요
            errorMessage = ERROR_NOT_CONNECT_INTERNET
            break
        default:
            //네트워크를 연결에 오류가 발생했습니다. 문의해주세요.
            errorMessage = ERROR_ETC
            break
        }
        let alert = AlertView(title: nil, message: errorMessage, okTitle: "확인", okStyle: .default, okCompletion: {
            exit(0)
        }, cancelCompletion: nil)
        rootVC?.present(alert, animated: true, completion: nil)
        return
    }
    
    func getMethod(_ str: String) -> HTTPMethod {
        var method: HTTPMethod?
        if str == POST {
            method = HTTPMethod.post
        } else if str == GET {
            method = HTTPMethod.get
        } else {
            method = HTTPMethod.post
        }
        return method!
    }
    
}
