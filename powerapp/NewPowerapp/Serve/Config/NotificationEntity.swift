//
//  NotificationEntity.swift
//  PowerApp
//
//  Created by Shawn Chun on 2015. 1. 26..
//  Copyright (c) 2015년 Koeracenter. All rights reserved.
//  coredate 데이터 테이블

import Foundation
import CoreData

@objc(NotificationEntity)
class NotificationEntity: NSManagedObject {
    @NSManaged var confirm: NSNumber
    @NSManaged var content: String
    @NSManaged var date: Date
    @NSManaged var key: String
    @NSManaged var directlink: String?
    @NSManaged var imagelink: String?
    @NSManaged var title: String?
    @NSManaged var type: String?
}
