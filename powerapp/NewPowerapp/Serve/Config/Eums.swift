//
//  Eums.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 4..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import Foundation

///하단메뉴 셀 태그들
enum BottomMenu: Int {
    case none = -1
    case back = 0
    case forward
    case main
    case cateory
    case basket
    case delivery
    case mypage
    case invite
    case review
    case push
    case sideMenu
    case reload
    case scrap
    case hiddenTag_YG
    
    //새로운 메뉴가 추가될 경우 커스텀 메뉴 위에 추가
    case custom1, custom2, custom3, custom4, custom5, custom6, custom7, custom8, custom9, custom10
}

///현재 어떤 뷰가 보여지고 있는지 확인용
enum OpenViewType {
    case notification
    case locationWeb
    case review
    case delivery
    case barcode
    case opensource
    case windowOpen
    case openShareWindow
    case shareSNS
    case baroTalk
    case main
    case keyword
}





//슬라이드 메뉴이미지
var SideHeaderImage: [String] = []

//슬라이드 메뉴
var SideHeaderCell: [String] = []


//enum SideHeaderCell: String {
//    case setting = "설정"
//    case editUserInfo = "회원정보수정"
//    case keyword = "키워드 알림"
//    case simpleJoin = "간편회원가입"
//    case normalJoin = "회원가입"
//    case review = "간편리뷰작성"
//    case delivery = "간편배송조회"
//    case invite = "친구 초대"
//    case notification = "알림 내용 보기"
//    case location = "위치서비스"
//    case version = "현재버전"
//    case jogunshop = "DAILY LOOK"
//}

enum SideMenuCellEnum: String {
    case notiSwitch = "알림스위치"
    case editUserInfo = "회원정보수정"
    case keyword = "키워드 알림"
    case simpleJoin = "간편회원가입"
    case normalJoin = "회원가입"
    case review = "간편리뷰작성"
    case delivery = "간편배송조회"
    case invite = "친구 초대"
    case notification = "알림 내용 보기"
    case location = "위치서비스"
    case version = "현재버전"
    case jogunshop = "DAILY LOOK"
}

///인트로 애니메이션
enum IntroType {
    case none
    case basic
    case slideInFromBottom
    case slideOutFromTop
    case swipeToRight
    case swipeToLeft
    case gettingBigger
    case gettingSmaller
}

///푸시 가상 태그
enum VirtualTag: String {
    case name = "[NAME]"
    case coupon = "[COUPON]"
    case reserve = "[RESERVE]"
    case point = "[POINT]"
    case emoney = "[EMONEY]"
}

enum TxtFile: String {
    //TODO: 사용중인 모든 텍스트파일 정리
    
    ///값 없음
    case none = ""
    ///유저아이디 저장
    case userID = "userID.txt"
    ///푸시 수신 여부
    case pushNotice = "pushNotice.txt"
    ///유저 핸드폰 번호 (사용 안함)
    case userPH = "userPH.txt"
    ///상점 정보 에러시 상점 아이디 API로 받온 상점의 새로운 도메인
    case shopInfoByShopID = "shopInfoByShopID.txt"
    ///토큰 존재 유무
    case isExistToken = "isExistToken.txt"
    ///UUID
    case uuid = "uuid.txt"
    ///푸시 수신 거부
    case noPushNotice = "noPushNotice.txt"
    ///
    case traceInflow = "traceInflow.txt"
    ///열어본 푸시의 rKey값들 1,2,3,4...
    case openKeyArray = "openKeyArray.txt"
    ///
    case traceNotification = "traceNotification.txt"
    ///자동로그인 사용 유무
    case autoLogin = "autoLogin.txt"
    ///유저 이름
    case userName = "userName.txt"
    ///토큰:
    ///\(uuid)|||\(TempVO.shared.deviceToken_str)|||\(shopID)|||\(userID)|||\(strAgree)|||\(auth_yn)|||\(userName)
    case insertToken = "insertToken.txt"
    ///앱스토어 애플 아이디, 없으면 빈값
    case appleId = "appleId.txt"
    ///키워드 알림 사용 유무
    case keyword_shop_yn = "keyword_shop_yn.txt"
    ///앱 전용 팝업 오늘 하루동안 보지 않기 옵션 선택 유무
    case popupOneDay = "popupOneDay.txt"
    ///SNS 로그인 종류 (사용 안함)
    case snsLoginType = "snsLoginType.txt"
    ///하단메뉴 사용 유무 Y, N
    case bottomDisplay = "bottomDisplay.txt"
    ///푸시 사용자 동의 Alert 여부
    case useAgreeNotiAlert = "useAgreeNotiAlert.txt"
    
    ///파일 경로 리턴
    func path() -> String {
        var path: String = ""
        let directoryDomains = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        if let directory = directoryDomains.first as NSString? {
            path = directory.appendingPathComponent(self.rawValue)
        }
        //        DFT_TRACE_PRINT(output: "\(self.rawValue) path: \(path)")
        return path
    }
    
    ///파일 존재 유무
    func isExist() -> Bool {
        let path = self.path()
        guard !path.isEmpty else { return false }
        
        let fileManager = FileManager.default
        let result = fileManager.fileExists(atPath: path)
        
        DFT_TRACE_PRINT(output: "\(self.rawValue) isExist: \(result)")
        return result
    }
    
    ///텍스트파일 삭제
    func remove() {
        let path = self.path()
        
        let fileManager = FileManager.default
        if self.isExist() {
            do {
                try fileManager.removeItem(atPath: path)
                DFT_TRACE_PRINT(output: "remove \(self.rawValue)")
            } catch {
                DFT_TRACE_PRINT(output: error.localizedDescription, error)
            }
        }
    }
    
    ///텍스트파일에 저장
    func set(_ value: String) {
        DFT_TRACE_PRINT(output: "\(self.rawValue) set: \(value)")
        
//        value.writeFile(to: self.rawValue)
    }
    
    ///텍스트파일 읽어 옴
    func getValue() -> String {
//        return self.rawValue.readFile()
        return "return"
    }
}

