//
//  Config.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 4..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import Foundation

class Config  {

    static var shared = Config()
    
    var introImage_url: String = "" // 인트로 이미지 URL
    var uuid: String = ""
    var adid: String = ""
    /*
     var shopURL: String = "www.jogunshop.com"
     var domain: String = "http://www.jogunshop.com"
     var shopID: String = "jogunshop"
     */
    /*
     var shopURL: String = "lsw.makemall.kr"
     var domain: String = "http://lsw.makemall.kr"
     var shopID: String = "lsw"
     */
    
    var shopID: String = "" {
        didSet {
            self.introImage_url = "\(self.domain)/shopimages/\(self.shopID)/powerapp_intro_android.png"
        }
    }
    var domain: String = ""
    var shopURL: String = "" {
        didSet {
            self.domain = "http://\(shopURL)"
            
            self.error_str = "<!DOCTYPE html><html lang='ko'><head><meta charset='utf-8'><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'>"
            self.error_str += "<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densitydpi=medium-dpi'>"
            self.error_str += "<meta name='apple-mobile-web-app-capable' content='no' />"
            self.error_str += "<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />"
            self.error_str += "<meta name='format-detection' content='telephone=no' /><title>네트워크 에러</title></head><style>"
            self.error_str += "html,body{width:100%;height:100%;color:#4d4d4d;padding:0;margin:0}"
            self.error_str += ".content{position:absolute;left:50%;top:50%;width:300px;height:150px;margin-left:-150px;margin-top:-75px;text-align:center;}"
            self.error_str += "</style><body><div class='content'><h4>잘못된 경로로 접근하였습니다.</h4>"
            self.error_str += "<p><a href='\(self.domain)/m/'>홈으로 이동해 주시기 바랍니다.</a></p></div></body></html>"
        }
    }
    
    var sslip: String = "www.makeshop.co.kr"
    
    var auth_type: String = ""
    var back_path: String = ""
    var basket_path: String = ""
    var category_path: String = ""
    var delivery_path: String = ""
    var enddate: String = ""
    var forward_path: String = ""
    var free_shop: Bool = true
    var home_path: String = ""
    var intro_android: String = ""
    var intro_ios: String = ""
    var invitation_benefit: String = ""
    var invite_path: String = ""
    var is_invitation: Bool = false
    var is_join: Bool = false
    var menu_color: String = ""
    var menu_path: String = ""
    var mypage_path: String = ""
    var password_rule: String = ""
    var popup_image: String = ""
    var popup_link: String = ""
    var push_benefit: String = ""
    var push_color: String = ""
    var push_path: String = ""
    var push_receive_day: String = ""
    var review_path: String = ""
    var server: String = ""
    var shopname: String = ""
    var shop_category: String = ""
    var update_date: String = ""                                // 이미지를 업로드한 날짜
    var use_back_btn: Bool = false
    var use_barcode: String = ""
    var use_basket_btn: Bool = false
    var use_category_btn: Bool = false
    var use_delivery_btn: Bool = false
    var use_forward_btn: Bool = false
    var use_home_btn: Bool = false
    var use_invitation: String = ""
    var use_invite_btn: Bool = false
    var use_menu_btn: Bool = false
    var use_mypage_btn: Bool = false
    var use_popup: String = "" {
        didSet {
            if use_popup == "YES" {
                is_popup = true
            }
        }
    }
    var is_popup: Bool = false
    var use_join: Bool = true
    var use_push_btn: Bool = false
    var use_review_btn: Bool = false
    var use_simple_join:String = ""
    var use_simple_review: String = "" {
        didSet {
            if use_simple_review == "YES" {
                is_review = true
            }
        }
    }
    var is_review:Bool = false
    var ios_version: String = ""
    var push_agree: String = ""
    var use_offline_push: Bool = false
    var sns_facebook: String = "N"
    var sns_kakao: String = "N"
    var sns_naver: String = "N"
    
    var custom_menu_1_path: String = ""
    var use_custom_menu_1_btn: Bool = false
    var custom_1_link: String = ""
    var custom_1_name: String = ""
    
    var custom_menu_2_path: String = ""
    var use_custom_menu_2_btn: Bool = false
    var custom_2_link: String = ""
    var custom_2_name: String = ""
    
    var custom_menu_3_path: String = ""
    var use_custom_menu_3_btn: Bool = false
    var custom_3_link: String = ""
    var custom_3_name: String = ""
    
    var custom_menu_4_path: String = ""
    var use_custom_menu_4_btn: Bool = false
    var custom_4_link: String = ""
    var custom_4_name: String = ""
    
    var custom_menu_5_path: String = ""
    var use_custom_menu_5_btn: Bool = false
    var custom_5_link: String = ""
    var custom_5_name: String = ""
    
    var custom_menu_6_path: String = ""
    var use_custom_menu_6_btn: Bool = false
    var custom_6_link: String = ""
    var custom_6_name: String = ""
    
    var custom_menu_7_path: String = ""
    var use_custom_menu_7_btn: Bool = false
    var custom_7_link: String = ""
    var custom_7_name: String = ""
    
    var custom_menu_8_path: String = ""
    var use_custom_menu_8_btn: Bool = false
    var custom_8_link: String = ""
    var custom_8_name: String = ""
    
    var custom_menu_9_path: String = ""
    var use_custom_menu_9_btn: Bool = false
    var custom_9_link: String = ""
    var custom_9_name: String = ""
    
    var custom_menu_10_path: String = ""
    var use_custom_menu_10_btn: Bool = false
    var custom_10_link: String = ""
    var custom_10_name: String = ""
    
    
    var use_refresh_btn: Bool = false
    var refresh_path: String = ""
    var bottom_display: String = "Y"
    var use_scrap_btn : Bool = false
    var scrap_path: String = ""
    var kakao_js_key:  String = ""
    var use_share_fb: Bool = false
    var use_share_tw: Bool = false
    var use_share_ka: Bool = false
    var use_share_ks: Bool = false
    
    var is_keyword : Bool = false // 슬라이드 키워드 알림 존재 유무
    
    var menu_num:Int = 0
    
    var error_str: String = ""
    
    var slide_color: String = "DEFAULT" //슬라이드 메뉴 디자인(색상) 설정 값 전달. (DEFAULT, BLACK, WHITE, GRAY)
    var menu_user_info: String = "N" //슬라이드 메뉴 상단에 회원정보 노출 설정 값 전달. (Y, N)
    var slide_type: String = "HALF" //슬라이드 메뉴 디자인 형태 설정 값 전달. (HALF, FULL)
    var slide_T_color: String = "BLACK"  //슬라이드 메뉴 타이틀 색상 설정 값 전달 (BLACK, WHITE, GRAY)
    var slide_T_align: String = "CENTER"    //슬라이드 메뉴 타이틀 정렬 값 전달 (CENTER, LEFT)
    //회원 포인트 (상점에서 사용하지 않는 경우 : "")
    var user_point: String = "" {
        didSet {
            user_point = numberDecimalStyleFormat(to: user_point)
        }
    }
    //회원 적립금 (상점에서 사용하지 않는 경우 : "")
    var user_reserve: String = "" {
        didSet {
            user_reserve = numberDecimalStyleFormat(to: user_reserve)
        }
    }
    //회원 쿠폰 갯수 (상점에서 사용하지 않는 경우 : "")
    var user_coupon: String = "" {
        didSet {
            user_coupon = numberDecimalStyleFormat(to: user_coupon)
        }
    }
    //회원 예치금 (상점에서 사용하지 않는 경우 : "")
    var user_emoney: String = "" {
        didSet {
            user_emoney = numberDecimalStyleFormat(to: user_emoney)
        }
    }
    var user_group: String = ""//회원 등급 (상점에서 사용하지 않는 경우, 회원 등급이 없는경우: "")
    
    var intro_image_array: [String] = [String]() //랜덤 인트로 이미지 주소 저장용 배열
    
    /// B2B상점인지 성인몰 인지 구분하기 위함
    ///adult_type : ADULT (성인몰), B2B (b2b샵), N (일반)
    var adult_type: String = "N"
    
    ///인트로 보여주는 시간
    var use_intro_time: String = "0"
    
    ///인트로 이미지 애니메이션 타입
    var use_intro_effect: String = "DISAPPEAR"
    
    ///키워드 알림 유무
    var use_keyword: Bool = false
    
    ///자리수 마다 콤마 표기
    func numberDecimalStyleFormat(to: String) -> String {
        let numberFomat = NumberFormatter()
        numberFomat.numberStyle = .decimal
        
        guard let strToInt = numberFomat.number(from: to) else {
            return to
        }
        guard let decimalStyle = numberFomat.string(from: strToInt) else {
            return to
        }
        
        let retrunString = "\(decimalStyle)"
        
        return retrunString
    }
    
    fileprivate init() {
    }

}
