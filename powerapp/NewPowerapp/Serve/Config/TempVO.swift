//
//  TempVO.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 4..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import Foundation

class NotiEventEntity {
    var confirm: Bool = true
    var rKey: String = ""
    var content: String = ""
    var link: String = ""
    
    init(rKey: String, content: String, confirm: Bool, link: String) {
        self.rKey = rKey
        self.content = content
        self.confirm = confirm
        self.link = link
    }
}

class BotMenu {
    var menuType = ""
    var tag = 0
    var fileName = ""
    var path = ""
    var basicImageName = ""
    var method = 0
    
    init(type: String, tag: Int) {
        self.menuType = type
        self.tag = tag
    }
}

class TempVO {
    static var shared = TempVO()
    
    let mKey: String = "power!make141212@!*powerapp^make"         // mKey 값
    let check_upload_host: String = "boardupload.makeshop.co.kr"  // 업로드 호스트
    
    ///하단메뉴 인덱스
    static let botMenuArr: [String] = [
        "back_path", "forward_path", "home_path", "category_path",
        
        "basket_path", "delivery_path", "mypage_path", "invite_path",
        
        "review_path", "push_path", "menu_path", "refresh_path",
        
        "scrap_path", "menu_hiddentag",
        //새로운 메뉴가 추가될 경우 커스텀 메뉴 위에 추가
        "custom_menu_1_path", "custom_menu_2_path", "custom_menu_3_path",
        "custom_menu_4_path", "custom_menu_5_path", "custom_menu_6_path",
        "custom_menu_7_path", "custom_menu_8_path", "custom_menu_9_path",
        "custom_menu_10_path",
        ]
    
    var botMenus: [BotMenu] = [BotMenu]()
    
    
    //Config 생성 속도를 생각해서 lazy선언함
    
    ///하단메뉴 순서 체크용
    lazy var botMenuTuples: [(String, String, String, String, Bool)] = [
        ("back_path", "powerapp_back.png", "menu_back",
         Config.shared.back_path, Config.shared.use_back_btn),
        
        ("forward_path", "powerapp_forward.png", "menu_forward",
         Config.shared.forward_path, Config.shared.use_forward_btn),
        
        ("home_path", "powerapp_home.png", "menu_home",
         Config.shared.home_path, Config.shared.use_home_btn),
        
        ("basket_path", "powerapp_basket.png", "menu_basket",
         Config.shared.basket_path, Config.shared.use_basket_btn),
        
        ("mypage_path", "powerapp_mypage.png", "menu_mypage",
         Config.shared.mypage_path,  Config.shared.use_mypage_btn),
        
        ("category_path", "powerapp_category.png", "menu_category",
         Config.shared.category_path, Config.shared.use_category_btn),
        
        ("delivery_path", "powerapp_delivery.png", "menu_delivery",
         Config.shared.delivery_path, Config.shared.use_delivery_btn),
        
        ("review_path", "powerapp_review.png", "menu_review",
         Config.shared.review_path, Config.shared.use_review_btn),
        
        ("invite_path", "powerapp_invite.png", "menu_invite",
         Config.shared.invite_path, Config.shared.use_invite_btn),
        
        ("push_path", "powerapp_push.png", "menu_push",
         Config.shared.push_path, Config.shared.use_push_btn),
        
        ("refresh_path", "powerapp_refresh.png", "menu_refresh",
         Config.shared.refresh_path, Config.shared.use_refresh_btn),
        
        ("menu_path", "powerapp_menu.png", "menu_more",
         Config.shared.menu_path, Config.shared.use_menu_btn),
        
        ("scrap_path", "powerapp_scrap.png", "menu_scrap",
         Config.shared.scrap_path, Config.shared.use_scrap_btn)
    ]
    
    
    ///커스텀 메뉴 10개
    lazy var botCustomMenuTuples: [(String, String, String, String, Bool, String)] = [
        ("custom_menu_1_path", "powerapp_custom_menu_1.png", "menu_custom_menu_1",
         Config.shared.custom_menu_1_path, Config.shared.use_custom_menu_1_btn, Config.shared.custom_1_name),
        
        ("custom_menu_2_path", "powerapp_custom_menu_2.png", "menu_custom_menu_2",
         Config.shared.custom_menu_2_path, Config.shared.use_custom_menu_2_btn, Config.shared.custom_2_name),
        
        ("custom_menu_3_path", "powerapp_custom_menu_3.png", "menu_custom_menu_3",
         Config.shared.custom_menu_3_path, Config.shared.use_custom_menu_3_btn, Config.shared.custom_3_name),
        
        ("custom_menu_4_path", "powerapp_custom_menu_4.png", "menu_custom_menu_4",
         Config.shared.custom_menu_4_path, Config.shared.use_custom_menu_4_btn, Config.shared.custom_4_name),
        
        ("custom_menu_5_path", "powerapp_custom_menu_5.png", "menu_custom_menu_5",
         Config.shared.custom_menu_5_path, Config.shared.use_custom_menu_5_btn, Config.shared.custom_5_name),
        
        ("custom_menu_6_path", "powerapp_custom_menu_6.png", "menu_custom_menu_6",
         Config.shared.custom_menu_6_path, Config.shared.use_custom_menu_6_btn, Config.shared.custom_6_name),
        
        ("custom_menu_7_path", "powerapp_custom_menu_7.png", "menu_custom_menu_7",
         Config.shared.custom_menu_7_path, Config.shared.use_custom_menu_7_btn, Config.shared.custom_7_name),
        
        ("custom_menu_8_path", "powerapp_custom_menu_8.png", "menu_custom_menu_8",
         Config.shared.custom_menu_8_path, Config.shared.use_custom_menu_8_btn, Config.shared.custom_8_name),
        
        ("custom_menu_9_path", "powerapp_custom_menu_9.png", "menu_custom_menu_9",
         Config.shared.custom_menu_9_path, Config.shared.use_custom_menu_9_btn, Config.shared.custom_9_name),
        
        ("custom_menu_10_path", "powerapp_custom_menu_10.png", "menu_custom_menu_10",
         Config.shared.custom_menu_10_path, Config.shared.use_custom_menu_10_btn, Config.shared.custom_10_name)
    ]
    
    
    
    
    var notiEventArr: [NotiEventEntity] = [NotiEventEntity]()
    
    var param_keyword = ""
    var sns_login_type = ""
    var mkey = ""
    var msecure_key = ""
    var deviceToken_str: String = ""
    var isShowAgreeNotiAlert: Bool = false
    var isShowNoAgreeNotiAlert: Bool = false
    var isActiveNotification: Bool = false
    var isReceivePushDirectlink: Bool = false
    
    var push_confirm: Bool = false
    var push_key: String = ""
    var push_content: String = ""
    
    var isMainPage: Bool = false
    var isSideMenu: Bool = false
//    var pushCell: PushCell?
//    var mainVC: MainViewController?         // 메인뷰 상태 체크
    
    var directURL_str: String = ""          // 다이렉트 링크 주소
    
    var user_id:String = ""                 // 로그인 모달과 회원가입 페이지에서 들어오는 아이디값을 공유하기 위해 쓰이는 변수
    var user_name: String = ""              // 로그인 했을때 회원 이름
    var returnURL_str: String = ""          // 로그인 했을 때 페이지 이동시킬 주소값
    var returnCloseURL_str: String = ""     // 비회원 보기 클릭했을 때 이동시킬 주소값
    //    var ph: String = "01011111111"
    var notiTextRightMargin: Float = 12.0
    var inflow_id: String = ""
    var inflow_date: String = ""
    var notification_key: String = ""
    var notification_date: String = ""
    var shopInfoData_str: String = ""
    var eucKrEncoding_str : String = ""          //euc-kr로 디코딩된 문자열
    var isInstall: Bool = false                  // 알림센터의 노티피케이션이 제대로 설정 되었는지 메인 화면 터치시 다시 확인
    
    var isChange_ImageUpdate_date : Bool = false //이미지 업데이트 날짜가 바뀌었는지 알려주는 변수
    
    var login_id_key : String = ""               //쿠키에서 가져오는 회원 아이디 암호화 문자열_key
    
    var nowIntroImage : String = "image_intro"   //현재 인트로 이미지
    
    var bottomMenuCellTag : Int = 10//뱃지를 붙이기 위한 하단메뉴 셀의 태그
    
    var isEnterForeground : Bool = false// 백그라운드에서 앱으로 들어온건지 확인용
    
    var isEnterNoitiCenter : Bool = false //알림센터에서 들어왔는지 확인용
    
    var localVersion: String = ""
    
    var storeVersion: String = ""
    
    //  연락처 호출
    //    func setP(_ val: String) -> Bool {
    //        if let dir: NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first as NSString? {
    //            let path = dir.appendingPathComponent("userPH.txt")
    //            do {
    //                let userPH = try String(contentsOfFile: path, encoding: .utf8)
    //                if (userPH != val) {
    //                    self.ph = val
    //                    return true
    //                }
    //            }
    //            catch {
    //                self.ph = val
    //            }
    //        }
    //        return false
    //    }
    
    func calDate(_ val: String, move: Int) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"             // 날짜 형식 설정
        let start = dateFormatter.date(from: val)
        let cal = Calendar.current
        var comps = (cal as NSCalendar).components([NSCalendar.Unit.year, NSCalendar.Unit.month, NSCalendar.Unit.day], from: start!)
        comps.month = comps.month! + move
        let end = cal.date(from: comps)
        return dateFormatter.string(from: end!)
    }
    
    func changeDateFormatter(_ val: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"         // 날짜 형식 설정
        let date = dateFormatter.date(from: val)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy.MM.dd HH:mm:ss"   // 날짜 형식 설정
        return dateFormatter2.string(from: date!)
    }
    
    fileprivate init() {
    }
}
