//
//  Database.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 6..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit
import SQLite

class Database {
    var db: Connection?
    let users = Table("users")
    
    let key = Expression<String>("key")
    let type = Expression<String>("type")
    let title = Expression<String>("title")
    let content = Expression<String>("content")
    let directlink = Expression<String>("directlink")
    let imagelink = Expression<String>("imagelink")
    let confirm = Expression<Bool>("confirm")
    let date = Expression<Date>("date")
    
    func createDB() {
        if let dirString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first {
            let path = dirString as NSString
            db = try! Connection("\(path)/db.sqlite3")
            
//            db?.trace { print($0) }
//        CREATE TABLE IF NOT EXISTS "users" ("key" TEXT NOT NULL, "type" TEXT NOT NULL, "title" TEXT NOT NULL, "content" TEXT NOT NULL, "directlink" TEXT NOT NULL, "imagelink" TEXT NOT NULL, "confirm" INTEGER NOT NULL, "date" TEXT NOT NULL)
            
            do {
                try db?.run(users.create(ifNotExists: true) { t in
                    t.column(key)
                    t.column(type)
                    t.column(title)
                    t.column(content)
                    t.column(directlink)
                    t.column(imagelink)
                    t.column(confirm)
                    t.column(date)
                })
            } catch {
                DFT_TRACE_PRINT(output: "databaseCreate_Error: \(error)")
            }
        }
        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//        let push_date = dateFormatter.date(from: "2018-09-10 11:11:11")!
//
//        try! db?.run(users.insert(key <- "1002", type <- "sms", title <- "[제목]", content <- "안녕하십니까?", directlink <- "useinfo.html", imagelink <- "http://image", confirm <- true, date <- push_date))
        
//        let rowid2 = try! db?.run(users.insert(email <- "test@mac.com"))
//                let cc = try! db?.run(alice.update(email <- email.replace("mac.com", with: "me.com")))
        //        print("!!! \(alice)")
        //        print("!!!!!alice.select(email): \(alice.select(email))")
        
        //        print("!!!alice: \(alice)")
        //
        
//        let alice = users.filter(key == "1002")
        
//        for user in (try! db?.prepare(alice))! {
//            print("id: \(user[id]), email: \(user[email])")
//            print(user[key])
//            let aa = user[key]
//            print(aa)
//        }
        
        
        
        
        
        //
        //        let emails = VirtualTable("emails")
        //
        //        let subject = Expression<String?>("subject")
        //        let body = Expression<String?>("body")
        //
        //        try! db?.run(emails.create(.FTS4(subject, body)))
        //
        //        try! db?.run(emails.insert(
        //            subject <- "Hello, world!",
        //            body <- "This is a hello world message."
        //        ))
        //
        //        let row = try! db?.pluck(emails.match("hello"))
        //
        //        let query = try! db?.prepare(emails.match("hello"))
        //        for row in query! {
        //            print(row[subject])
        //        }
        
        
    }
    
    //insert & update를 동시에 해야할꺼같음 (??)
    //기존의 powerapp의 coredata를 읽어와서 sqllite에 다시 덮어씌워준다.
    func insertDB(data: AnyObject) {
        //coredata
        let c_confirm = data.value(forKey: "confirm") as? Bool ?? false
        let c_content = data.value(forKey: "content") as? String ?? ""
        let c_date = data.value(forKey: "date") as? Date ?? Date()
        let c_directlink = data.value(forKey: "directlink") as? String ?? ""
        let c_imagelink = data.value(forKey: "imagelink") as? String ?? ""
        let c_key = data.value(forKey: "key") as? String ?? ""
        let c_title = data.value(forKey: "title") as? String ?? ""
        let c_type = data.value(forKey: "type") as? String ?? ""
        
        do {
            try db?.run(users.insert(
                key <- c_key,
                type <- c_type,
                title <- c_title,
                content <- c_content,
                directlink <- c_directlink,
                imagelink <- c_imagelink,
                confirm <- c_confirm,
                date <- c_date
            ))
        } catch {
            DFT_TRACE_PRINT(output: "databaseInsert_Error: \(error)")
        }
    }
    
    func selectDB() {
        for user in try! (db?.prepare(users))! {
            print("name: \(try! user.get(key))")
        }
        
    }
}
