//
//  Util.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 4..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit
import WebKit

extension UIViewController {
    
}

//url 유효성검사
func encodedURL(string: String) -> URL? {
    guard let result_url = URL(string: string) else {
        let url_encoded = string.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? string
        return URL(string: url_encoded)
    }
    return result_url
}

//기본 alert
func AlertView(title: String? = "", message: String, okTitle: String, cancelTitle: String? = "", okStyle: UIAlertActionStyle? = .default, cancelStyle: UIAlertActionStyle? = .default, okCompletion: (() -> Void)?, cancelCompletion: (() -> Void)?) -> UIAlertController {
    let alert = UIAlertController(title: title ?? "", message: message, preferredStyle: .alert)
    if cancelTitle != "" {
        let cancelButton = UIAlertAction(title: cancelTitle, style: cancelStyle!) { action -> Void in
            cancelCompletion!()
        }
        alert.addAction(cancelButton)
    }
    let okButton = UIAlertAction(title: okTitle, style: okStyle!) { action -> Void in
        okCompletion!()
    }
    alert.addAction(okButton)
    return alert
}

//기본 로딩바
func AlertIndicator() -> UIActivityIndicatorView {
    let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
    
    let currentVC = UIApplication.shared.delegate?.window??.rootViewController?.topMostViewController()
    if currentVC != nil {
        currentVC?.view.addSubview(indicator)
        indicator.frame = (currentVC?.view.frame)!
    }
    
    
//    rootVC?.view.addSubview(indicator)
//    let aa = UIScreen.main.bounds
//    indicator.frame = aa
//    if let bounds = UIScreen.main.bounds {
//        indicator.frame = bounds
//    }
    
    return indicator
}

//현재시간 가져오기
func getNowTimeString() -> String {
    let now = Date()                               // 현재 날짜 및 시간 체크
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // 날짜 형식 설정
    return dateFormatter.string(from: now)
}


///아이튠즈 검색 API로 앱 버전 검색
func itunesLookup(country: String = "us", tryGetAppleID: Bool, completion: (() -> Void)?) {
    let bundleID = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier") as! String
    let lookupUrlStr = "http://itunes.apple.com/lookup"
    let lookupURL = URL(string: lookupUrlStr)
    var tryAppleID = tryGetAppleID
    
    let param : [String: String] = [
        "bundleId": bundleID,
        "country" : country
    ]
    
    DataDispatch().getJsonData(url: lookupUrlStr, method: GET, parm: param,
        success: { (response)-> Void in
            defer {
                if tryAppleID {
                    tryAppleID = false
                    itunesLookup(country: "kr", tryGetAppleID: false, completion: completion)
                } else {
                    completion?()
                }
            }
            
            let jsonResult = response
            guard let resultCount = (jsonResult as AnyObject).value(forKey: "resultCount") as? Int, resultCount > 0 else {
                return
            }
            guard let results = (jsonResult as AnyObject).value(forKey: "results") as? NSArray, results.count > 0 else {
                return
            }
            
            for result in results {
                let resultObject = result as AnyObject
                if let appleIdNumber = resultObject.value(forKey: "trackId") as? Int {
                    UserDefaults.standard.set(String(appleIdNumber), forKey: .appleID)
                    tryAppleID = false
                }
                let appleID = UserDefaults.standard.string(forKey: .appleID)
                DFT_TRACE_PRINT(output: "apple id = \(appleID ?? "")))")
                let compareVer = (result as AnyObject).value(forKey: "version") as? String ?? ""
                TempVO.shared.storeVersion = compareVer
            }
    },  fail: { (response)->Void in
        DFT_TRACE_PRINT(output: "itunesLookup error: \(response)")
    })
}

///현재 앱 버전과 앱스토어에 앱 버전 비교
func isNewestVersionApp(local currentVer: String, server compareVer: String) -> Bool {
    switch compareVer.compare(currentVer) {
    case .orderedAscending:
        DFT_TRACE_PRINT(output:"앱스토어 보다 앱이 최신 버전입니다. app = \(currentVer) , AppStore = \(compareVer)")
        return true
    case .orderedSame:
        DFT_TRACE_PRINT(output:"앱스토어와 같은 버전입니다. app = \(currentVer) , AppStore = \(compareVer)")
        return true
    case .orderedDescending:
        DFT_TRACE_PRINT(output:"앱스토어 보다 낮은 버전입니다. app = \(currentVer) , AppStore = \(compareVer)")
        return false
    }
}

//세션체크
func checkSessionHandler(webView: WKWebView? = nil ,_ completion: @escaping (_ result: Bool) -> Void) {
    var resultValue = false
    
    webView?.evaluateJavaScript("checkPowerappLoginState()") { (result, error) -> Void in
        guard let jsonResult = result as? NSDictionary else {
            completion(resultValue)
            return
        }
        //로그인 성공시 로그인 아이디키값을 저장하고 로그인여부를 리턴해준다
        if let loginCheck = jsonResult.value(forKey: "login") as? String {
            if loginCheck == "Y" {
                resultValue = true
                if let loginId = jsonResult.value(forKey: "login_id_key") as? String {
                    TempVO.shared.login_id_key = loginId
                }
            }
        }
        if (error != nil) {
            DFT_TRACE_PRINT(output:"checkSessionHandler error : \(error!.localizedDescription) ")
        }
        DFT_TRACE_PRINT(output:"체크세션 리턴: \(resultValue)")
        completion(resultValue)
    }
}

//슬라이드 타입체크
func getSlideType() {
    if Config.shared.slide_type == SLIDE_DEFAULT {
        SideHeaderImage = ["Image_setting", "Image_easy", "Image_push", "Image_version"]
        SideHeaderCell = ["설정", "간편기능", "앱전용 알림/혜택", "버전"]
    } else {
        SideHeaderCell = ["설정", "간편기능", "버전"]
        if Config.shared.slide_color == SLIDE_WHITE {
            SideHeaderImage = ["ic_setting_bl", "ic_refresh_bl", "ic_version_bl"]
        } else {
            SideHeaderImage = ["ic_setting_wh", "ic_refresh_wh", "ic_version_wh"]
        }
    }
}

//text height구하기
func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat {
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    
    label.sizeToFit()
    return label.frame.height
}

//url string으로 UIImage Height가져오기
func getURLImageHeight(urlString: String) -> CGFloat {
    var imgHeight: CGFloat = 0.0
    let url = URL(string: urlString)
    do {
        let data = try Data(contentsOf: url!)
        let image = UIImage(data: data)
        //이미지 리사이징
        if image?.size.width ?? 0.0 > UIScreen.main.bounds.width {
            imgHeight = (UIScreen.main.bounds.width * (image?.size.height)!) / (image?.size.width)!
        } else {
            imgHeight = image?.size.height ?? 0.0
        }
    }catch let err {
        print("Error : \(err.localizedDescription)")
    }
    return imgHeight
}

///////////////////////////////////extension///////////////////////////////////////////

extension UIView {
    ///iPhone X 구분
    func isiPhoneXScreen() -> Bool {
        guard #available(iOS 11.0, *) else {
            return false
        }
        
        return UIApplication.shared.windows[0].safeAreaInsets != UIEdgeInsets.zero
    }
    
    //바로톡 아래에서 위로 나타내는 애니메이션, 아래로 사라지는 애니메이션
    // Name this function in a way that makes sense to you...
    // slideFromLeft, slideRight, slideLeftToRight, etc. are great alternative names
    
    ///아래에서 위로 나타내는 애니메이션
    func slideInFromBottom(duration: TimeInterval = 0.3, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromBottomTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate = completionDelegate as? CAAnimationDelegate {
            slideInFromBottomTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromBottomTransition.type = kCATransitionPush
        slideInFromBottomTransition.subtype = kCATransitionFromTop
        slideInFromBottomTransition.duration = duration
        slideInFromBottomTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        slideInFromBottomTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromBottomTransition, forKey: "slideInFromBottomTransition")
    }
    
    ///아래로 사라지는 애니메이션
    func slideOutFromTop(duration: TimeInterval = 0.3, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let slideInFromTopTransition = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate = completionDelegate as? CAAnimationDelegate {
            slideInFromTopTransition.delegate = delegate
        }
        
        // Customize the animation's properties
        slideInFromTopTransition.type = kCATransitionPush
        slideInFromTopTransition.subtype = kCATransitionFromBottom
        slideInFromTopTransition.duration = duration
        slideInFromTopTransition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        slideInFromTopTransition.fillMode = kCAFillModeRemoved
        
        // Add the animation to the View's layer
        self.layer.add(slideInFromTopTransition, forKey: "slideInFromTopTransition")
    }
    
    ///왼쪽에서 오른쪽으로 나타나는 애니메이션
    func swipeToRight(duration: TimeInterval = 0.3, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let swipeToRightAnimation = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate = completionDelegate as? CAAnimationDelegate {
            swipeToRightAnimation.delegate = delegate
        }
        
        // Customize the animation's properties
        swipeToRightAnimation.type = kCATransitionPush
        swipeToRightAnimation.subtype = kCATransitionFromLeft
        swipeToRightAnimation.duration = duration
        swipeToRightAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        // Add the animation to the View's layer
        self.layer.add(swipeToRightAnimation, forKey: "swipeToRight")
    }
    
    ///오른쪽에서 왼쪽으로 나타나는 애니메이션
    func swipeToLeft(duration: TimeInterval = 0.3, completionDelegate: AnyObject? = nil) {
        // Create a CATransition animation
        let swipeToLeftAnimation = CATransition()
        
        // Set its callback delegate to the completionDelegate that was provided (if any)
        if let delegate = completionDelegate as? CAAnimationDelegate {
            swipeToLeftAnimation.delegate = delegate
        }
        
        // Customize the animation's properties
        swipeToLeftAnimation.type = kCATransitionPush
        swipeToLeftAnimation.subtype = kCATransitionFromRight
        swipeToLeftAnimation.duration = duration
        swipeToLeftAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        // Add the animation to the View's layer
        self.layer.add(swipeToLeftAnimation, forKey: "swipeToLeft")
    }
    
    func gettingBigger(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration:duration, delay: delay, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 2, y: 2)
            self.alpha = 0
        }, completion: {(finished) in
            self.transform = CGAffineTransform.identity
            completion(finished)
        })
    }
    
    func gettingSmaller(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration:duration, delay: delay, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            self.alpha = 0
        }, completion: completion)
    }
    
    func fadeIn(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration:duration, delay: delay, options: .curveEaseInOut, animations: {
            self.alpha = 1.0
        }, completion: completion)
    }
    
    func fadeOut(duration: TimeInterval = 1.0, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: .curveEaseInOut, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
}

extension UIViewController {
    
    //컨트롤뷰 띄워주기
    func presentVC(_ vc: String) -> UIViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: vc) as UIViewController
        vc.modalPresentationStyle = .overFullScreen
        return vc
    }
    
    func presentDetail(_ vc: UIViewController, _ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 5
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        vc.view.window!.layer.add(transition, forKey: kCATransition)
        present(viewControllerToPresent, animated: false)
    }
    
    //최상단 뷰컨트롤러 가져오기
    func topMostViewController() -> UIViewController {
        
        if let presented = self.presentedViewController {
            return presented.topMostViewController()
        }
        
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController?.topMostViewController() ?? navigation
        }
        
        if let tab = self as? UITabBarController {
            return tab.selectedViewController?.topMostViewController() ?? tab
        }
        
        return self
    }
}

extension UIColor {
    convenience init(hexColorCode: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if hexColorCode.hasPrefix("#") {
            let index   = hexColorCode.index(hexColorCode.startIndex, offsetBy: 1)
            let hex     = String(hexColorCode[index...])
            
            let scanner = Scanner(string: String(hex))
            var hexValue: CUnsignedLongLong = 0
            
            if scanner.scanHexInt64(&hexValue) {
                if hex.count == 6 {
                    red   = CGFloat((hexValue & 0xFF0000) >> 16) / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)  / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF) / 255.0
                } else if hex.count == 8 {
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                } else {
                    DFT_TRACE_PRINT(output:"invalid hex code string, length should be 7 or 9")
                }
            } else {
                DFT_TRACE_PRINT(output:"scan hex error")
            }
        } else {
            DFT_TRACE_PRINT(output:"invalid hex code string, missing '#' as prefix")
        }
        
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}

