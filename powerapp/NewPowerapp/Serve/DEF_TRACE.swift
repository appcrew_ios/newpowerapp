//
//  DEF_TRACE.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 3..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit

func DFT_TRACE_PRINT(filename: String=#file, line: Int=#line, funcname: String=#function, output: Any...) {
    #if DEBUG
    let now = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "ko_KR") // 로케일 설정
    dateFormatter.dateFormat = "yyyy-MM-dd hh:mm:ss.SSSS" // 날짜 형식 설정
    let dateNow = dateFormatter.string(from: now)
    //        print("[\(now.description)][\(filename)][\(funcname)][Line \(line)] \(output)")
    //print("[\(output) [\(funcname)][Line \(line)] \n-[\(filename)]\n")
    if output.count < 2 {
        if let printOut = output.first {
            NSLog("< \(dateNow): /* \(printOut) */ \(funcname) [Line: \(line)] \n \(filename)\n")
        }
    } else {
        NSLog("< \(dateNow): /* ")
        for i in output{
            if let log = i as? String {
                NSLog(log)
            }
        }
        NSLog(" */ \(funcname) [Line: \(line)] \n \(filename)\n")
    }
    #endif
}
