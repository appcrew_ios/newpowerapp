//
//  DataShare.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 3..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit

extension UserDefaults : StringDefaultSettable {
    enum StringKey : String {
        case test
        case tranceFlowId //어디 url을 타고 들어왔는지
        case tranceFlowDate //어디 url을 타고 들어왔는지
        case tranceNotiKey //푸시 rkey
        case tranceNotiDate //푸시 날짜
        case timeNoti //푸시 날짜 (위에랑 뭔 차이인지는 차차 ??)
        case shopInfoByShopID
        case appleID //apple아이디
        case userID
        case keyword //푸시허용하는 키워드
    }
}

protocol KeyNamespaceable {
    func namespaced<T : RawRepresentable>(_ key: T) -> String
}

extension KeyNamespaceable {
    func namespaced<T: RawRepresentable>(_ key: T) -> String {
        return "\(Self.self).\(key.rawValue)"
    }
}

protocol StringDefaultSettable : KeyNamespaceable {
    associatedtype StringKey : RawRepresentable
}

extension StringDefaultSettable {
    func set(_ value: String, forKey key: StringKey) {
        let key = namespaced(key)
        UserDefaults.standard.set(value, forKey: key)
    }
    
    func string(forKey key: StringKey) -> String? {
        let key = namespaced(key)
        return UserDefaults.standard.string(forKey: key)
    }
    
    func remove(forKey key: StringKey) {
        let key = namespaced(key)
        return UserDefaults.standard.removeObject(forKey: key)
    }
}
