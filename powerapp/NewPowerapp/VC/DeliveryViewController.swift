//
//  DeliveryViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 18..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit
import WebKit

class DeliveryViewController: UIViewController {

    @IBOutlet weak var webView: UIView!
    var deliveryWKWebView: WKWebView!
    var webviewUrl: String = "http://pushapp2.makeshop.co.kr/api/delivery/delivery.html"
    var deliveryData: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        deliveryWKWebView = WKWebView(frame: createWKWebViewFrame(size: webView.frame.size))
        webView.addSubview(deliveryWKWebView)
        deliveryWKWebView.navigationDelegate = self
        deliveryWKWebView.uiDelegate = self
        
        let url: URL = URL(string: "\(webviewUrl)")!
        let request = URLRequest(url: url)
        deliveryWKWebView.load(request)
        
        loadData()
    }
    
    func loadData() {
        let mKey: String = AES256Util.enc(with: "\(Config.shared.uuid)|make_delivery", key: TempVO.shared.mKey)
        var userID: String = ""
//        UserDefaults.standard.string(forKey: .userID) 현재는 userID를 저장안했음
        let db_userID = "maketest"
        if db_userID != "" {
            userID = (db_userID.data(using: .utf8, allowLossyConversion: true)?.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0)))!
        }
        
        // 날짜 값 가져오기 기간은 3개월까지
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMdd"
        let end_date =  dateFormatter.string(from: Date())
        let start_date = TempVO.shared.calDate(end_date, move: -3)
        
        let parameters_delivery: [String: String] = [
            "m_key": mKey,
            "admin_id": Config.shared.shopID,
            "type": "find_delivery",
            "user_id": userID,
            "start_date": start_date,
            "end_date": end_date
        ]
        
        let deliver_url = Config.shared.domain+"/list/API/powerapp_info.html"
        DataDispatch().getJsonData(url: deliver_url, method: POST, parm: parameters_delivery,
            success: { (response)-> Void in
                guard let jsonResult = response as? NSDictionary else {
                    return
                }

                if let result = jsonResult.value(forKey: "result") as? Bool, result {
                    if let data = jsonResult.value(forKey: "data") {
                        do {
                            let data1 =  try JSONSerialization.data(withJSONObject: data, options: JSONSerialization.WritingOptions.init(rawValue: 0)) // first of all convert json to the data
                            self.deliveryData = String(data: data1, encoding: String.Encoding.utf8)! // the data will be converted to the string
                        } catch let myJSONError {
                            DFT_TRACE_PRINT(output: myJSONError.localizedDescription)
                        }
                    }
                }
        },  fail: { (response)->Void in
            print("response: \(response)")
        })
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.setPositionOrientate()
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            DFT_TRACE_PRINT(output:"rotation completed")
        })
    }
    
    func setPositionOrientate() {
        deliveryWKWebView.frame = CGRect(x: 0, y: 0, width: webView.frame.width, height: webView.frame.height)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //종료
    @IBAction func clickCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension DeliveryViewController {
    fileprivate func createWKWebViewFrame(size: CGSize) -> CGRect {
        let navigationHeight: CGFloat = 0
        let toolbarHeight: CGFloat = 0
        let height = size.height - navigationHeight - toolbarHeight
        return CGRect(x: 0, y: 0, width: size.width, height: height)
    }
}


//WKnavigationDelegate - 페이지의 start, loadding, finish, error의 이벤트를 캐치할 수 있으며 웹페이지의 전반적인 상황을 확인할 수 있음
extension DeliveryViewController: WKNavigationDelegate {
    
    //탐색을 허용할지 취소할지 여부결정 (1)
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    //웹 콘텐츠가 로드되기 시작할때 (2)
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
    }
    
    //응답이 알려진 후 탐색을 허용할지 아니면 취소할지 결정 (3)
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        decisionHandler(.allow)
    }
    
    //웹보기에서 웹 콘텐츠를 받기 시작할때 (4)
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("!!!!")
    }
    
    //완료될때 호출 (5)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let js_str = "javascript:insertData(\(deliveryData));"
        deliveryWKWebView?.evaluateJavaScript(js_str, completionHandler: nil)
    }
    
    //웹보기가 서버 리다이렉션을 수신할때
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        print("!!!!")
    }
    
    //네비게이션중에 에러발생
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        
    }
    
    //컨텐츠를 로드하는중에 오류
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("!!!!")
    }
    
    //종료될때 호출
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print("!!!!")
    }
}

//WKUIDelegate - Javascript 이벤트를 캐치하여 동작합니다.
extension DeliveryViewController: WKUIDelegate {
    
    //새 웹보기를 만듭니다.
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        return nil
    }
    
    // - MARK: 자바스크립트 Alert 관련 메소드
    
    //JavaScript 경고 패널을 표시합니다.
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: (@escaping () -> Void)) {
        print("message: \(message)")
        completionHandler()
    }
    
    //JavaScript 확인 ​​패널을 표시합니다.
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: (@escaping (Bool) -> Void)) {
        print("!!!")
        completionHandler(true)
    }
    
    //JavaScript 텍스트 입력 패널을 표시합니다.
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        print("!!!")
        completionHandler(nil)
    }
}

