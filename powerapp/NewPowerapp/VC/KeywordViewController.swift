//
//  KeywordViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 10/10/2018.
//  Copyright © 2018 koreacenter.com. All rights reserved.
//

import UIKit

class KeywordViewController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var keywordCountLabel: UILabel!
    @IBOutlet weak var keywordSwitch: UIButton!
    @IBOutlet weak var keywordTextFiled: UITextField!
    @IBOutlet weak var keywordTableView: UITableView!
    
    var keywords = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //저장된 키워드가 있으면 세팅
        if let keywordArr = UserDefaults.standard.value(forKey: "keywordArr") as? [String] {
            keywords = keywordArr
            keywordCountLabel.text = "등록 키워드 (\(keywords.count)/10)"
        }
        
        //알람버튼 기본세팅
        if let isKeyword = UserDefaults.standard.string(forKey: .keyword) {
            if isKeyword == "Y" {
                keywordSwitch.isSelected = true
                keywordSwitch.setImage(UIImage(named: "toggle_on"), for: .selected)
            } else {
                keywordSwitch.isSelected = false
                keywordSwitch.setImage(UIImage(named: "toggle_off"), for: UIControlState())
            }
        } else {
            keywordSwitch.isSelected = false
            keywordSwitch.setImage(UIImage(named: "toggle_off"), for: UIControlState())
        }
    }
    
    //추가버튼 클릭
    @IBAction func clickAddButton(_ sender: Any) {
        inputKeyword()
    }
    //Done버튼 클릭
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        inputKeyword()
        return true
    }
    
    //키워드 수신허용 on/off
    @IBAction func clickSwitchButton(_ sender: Any) {
        if keywordSwitch.isSelected {
            keywordSwitch.setImage(UIImage(named: "toggle_off"), for: UIControlState())
            keywordSwitch.isSelected = false
            UserDefaults.standard.set("N", forKey: .keyword)
        } else {
            keywordSwitch.setImage(UIImage(named: "toggle_on"), for: .selected)
            keywordSwitch.isSelected = true
            UserDefaults.standard.set("Y", forKey: .keyword)
        }
        savekeywordData()
    }
    
    //종료하기
    @IBAction func clickCloseButton(_ sender: Any) {
        keywordTextFiled.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    func inputKeyword() {
        var error = ""
        let keywordStr = keywordTextFiled.text ?? ""
        
        if let isKeyword = UserDefaults.standard.string(forKey: .keyword) {
            if isKeyword == "N" {
                error = "키워드 수신 버튼을 ON으로 설정해주세요."
            }
        }
        
        if keywordStr == "" {
            error = "추가하려는 키워드를 입력하세요."
        }
        
        if keywords.count >= 10 {
            error = "등록 가능한 키워드의 개수는 10개 입니다."
        }
        
        for keyword in keywords {
            if keyword == keywordStr {
                error = "중복되는 키워드가 있어 추가되지 않습니다."
            }
        }
        
        keywordTextFiled.text = ""
        
        if error != "" {
            let alert = AlertView(title: nil, message: "\(error)", okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        } else {
            keywords.append(keywordStr)
            savekeywordData()
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let checkLetters = checkSpecialLettersAndSpace(in: string)
        let check = checkLength(in: string, with: checkLetters)

        let result = check.result
        let error = check.errorMessage

        if !result {
            let alert = AlertView(title: nil, message: error, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        }
        
        return result
    }
    
    ///특수문자, 공백 확인
    func checkSpecialLettersAndSpace(in string: String) -> (result: Bool, errorMessage: String) {
        var result = false
        var errorMessage = ""
        
        let components = string.components(separatedBy: .letters)
        let filtered = components.joined(separator: "")
        
        if filtered == string {
            let components2 = string.components(separatedBy: .decimalDigits)
            let filtered2 = components2.joined(separator: "")
            
            if filtered2 == string {
                // 백스페이스는 패스
                let char = string.cString(using: String.Encoding.utf8)!
                let isBackSpace = strcmp(char, "\\b")
                
                if isBackSpace == -92 {
                    result = true
                } else {
                    errorMessage = "특수문자 및 공백은 입력 할 수 없습니다."
                }
            } else {
                result = true
            }
        } else {
            result = true
        }
        
        return (result, errorMessage)
    }
    
    ///문자열 길이 확인
    func checkLength(in string: String, with value: (Bool, String)) -> (result: Bool, errorMessage: String) {
        var result = value.0
        var errorMessage = value.1
        
        guard result else { return value }
        
        let len = keywordTextFiled.text?.count ?? 0
        if len > 14 {
            // 백스페이스는 패스
            let char = string.cString(using: String.Encoding.utf8)!
            let isBackSpace = strcmp(char, "\\b")
            
            if isBackSpace == -92 {
                result = true
            }
            else {
                errorMessage = "한글, 영문, 숫자 포함 최대 입력 가능 글자는 15개 입니다."
                result = false
            }
        }
        return (result, errorMessage)
    }
    
    ///키워드 데이터 저장 및 전송
    func savekeywordData() {
        var keyword_str = ""
        var keyword_yn = ""
        
        for (index, keyStr) in keywords.enumerated() {
            if index == 0 {
                keyword_str = keyStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                continue
            }
            keyword_str.append("|||\(keyStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)")
        }

        if keywordSwitch.isSelected {
            keyword_yn = "Y"
        } else {
            keyword_yn = "N"
        }
        
        keywordCountLabel.text = "등록 키워드 (\(keywords.count)/10)"
        UserDefaults.standard.set(keywords, forKey: "keywordArr")
        keywordTextFiled.resignFirstResponder()

        sendToPushServer(keyword: keyword_str, with: keyword_yn)
    }
    
    ///푸시서버로 키워드, 알림 여부 전송
    func sendToPushServer(keyword: String, with keyword_yn: String) {
        let mKey: String = AES256Util.enc(with: "powerapp|\(Config.shared.uuid)||||\(keyword)", key: TempVO.shared.mKey)
        let parameters_keyword = [
            "shop_id": "\(Config.shared.shopID)",
            "uuid": "\(Config.shared.uuid)",
            "keycode": mKey,
            "keyword_yn": keyword_yn,
            "shop_keyword_yn": "Y"
        ]
        let keyword_url = "http://pushapp.makeshop.co.kr/api/insert_keyword.html"
        DataDispatch().getStringData(url: keyword_url, method: POST, parm: parameters_keyword,
                                   success: { (response)-> Void in
                                    DFT_TRACE_PRINT(output: "success: \(response)")
                                    self.keywordTableView.reloadData()
        },  fail: { (response)->Void in
            let alert = AlertView(title: nil, message: response, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return keywords.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "keywordCell") as? KeywordCell
        cell?.contentLabel.text = keywords[indexPath.row]
        if keywordSwitch.isSelected {
            cell?.contentLabel.textColor = .black
        } else {
            cell?.contentLabel.textColor = .lightGray
        }
        cell?.keywordVC = self
        cell?.index_path = indexPath
        return cell!
    }
    
    //키워드 삭제하기
    func deleteKeyword(to thisPath: IndexPath) {
        if keywordSwitch.isSelected {
            keywords.remove(at: thisPath.row)
            savekeywordData()
        } else {
            return
        }
    }
}

class KeywordCell: UITableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    lazy var index_path = IndexPath()
    var keywordVC:KeywordViewController?
    
    @IBAction func delKeyword(_ sender: UIButton) {
        keywordVC?.deleteKeyword(to: index_path)
    }
}
