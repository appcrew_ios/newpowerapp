//
//  OpenSourceViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 11/10/2018.
//  Copyright © 2018 koreacenter.com. All rights reserved.
//

import UIKit

class OpenSourceViewController: UIViewController {

    @IBOutlet weak var textView_width_const: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        textView_width_const.constant = self.view.frame.width
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.setPositionOrientate()
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            DFT_TRACE_PRINT(output:"rotation completed")
        })
    }
    
    func setPositionOrientate() {
        textView_width_const.constant = UIScreen.main.bounds.size.width-30
        let orient = UIApplication.shared.statusBarOrientation
        if view.isiPhoneXScreen() && orient.isLandscape {
            // 가로모드에서 세이프에어리어 크기 만큼 축소
            textView_width_const.constant -= 88
        }
    }
    
    @IBAction func clickCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setPositionOrientate()
    }
}
