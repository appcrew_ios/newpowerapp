//
//  PushViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 16/10/2018.
//  Copyright © 2018 koreacenter.com. All rights reserved.
//

import UIKit
import CoreData
import FontAwesome_swift

class PushViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var pushTableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    @IBOutlet weak var closeButton: UIBarButtonItem!
    
    var prevCell: PushTableCell? //이전에 클릭한 셀을 저장하여 공유셀을 지워주기 위해 만들었음
    var serverDatas = [Notification]()
    let fontAwesomeAttributes: [NSAttributedStringKey: UIFont] = [.font: UIFont.fontAwesome(ofSize: 24)]

//        closeButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
//        closeButton.title = String.fontAwesomeIcon(name:.trash)

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        pushTableView.allowsMultipleSelectionDuringEditing = true
//        pushTableView.allowsMultipleSelection = true
//        pushTableView.allowsMultipleSelectionDuringEditing = true
//        pushTableView.separatorStyle = .none
        
        //네이게이션 버튼 초기화
        closeButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
        closeButton.setTitleTextAttributes(fontAwesomeAttributes, for: .selected)
        closeButton.title = String.fontAwesomeIcon(name: .close)
        
        editButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
        editButton.setTitleTextAttributes(fontAwesomeAttributes, for: .selected)
        editButton.title = String.fontAwesomeIcon(name: .trash)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPushData()
    }
    
    
    func viewInit() {
        if serverDatas.count > 0 {
            pushTableView.reloadData()
        } else {
            let alert = AlertView(title: nil, message: "푸시 내용이 없습니다.\n푸시를 터치하신 내용만 저장됩니다.", okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func getPushData() {
        let mKey: String = AES256Util.enc(with: "powerapp|\(Config.shared.uuid)", key: TempVO.shared.mKey)
        let parameters_noti_history = [
            "shop_id": "\(Config.shared.shopID)",
            "uuid": "\(Config.shared.uuid)",
//            "search_date":  date_noti, // "2017-04-24",
            "search_date":  "2018-09-21", // "2017-04-24",
            "keycode": mKey
        ]
        let history_url = "http://pushapp.makeshop.co.kr/api/push_history_list_new.html"
        DataDispatch().getJsonData(url: history_url, method: POST, parm: parameters_noti_history,
            success: { (response)-> Void in
                let jsonResult = NSMutableArray(array: response as! NSArray)
                if jsonResult.count > 0 {
                    print("!!!!!!!!!!!!!!!!!!!! \(jsonResult)")
                    for json in jsonResult {
                        
                        let data =  Notification()
                        data.changeDate = (json as AnyObject).value(forKey: "chg_date") as? Date ?? nil
                        data.imgUrl = (json as AnyObject).value(forKey: "img_url") as? String ?? ""
                        data.linkUrl = (json as AnyObject).value(forKey: "link_url") as? String ?? ""
                        data.msg = (json as AnyObject).value(forKey: "msg") as? String ?? ""
                        data.notiType = (json as AnyObject).value(forKey: "noti_type") as? String ?? ""
                        data.rKey = (json as AnyObject).value(forKey: "rkey") as? String ?? ""
                        data.startDate = (json as AnyObject).value(forKey: "start_date") as? String ?? ""
                        data.targetType = (json as AnyObject).value(forKey: "target_type") as? String ?? ""
                        data.title = (json as AnyObject).value(forKey: "title") as? String ?? ""
                        
                        
                        print("==========================================")
                        print(data.changeDate)
                        print(data.imgUrl)
                        print(data.linkUrl)
                        print(data.msg)
                        print(data.notiType)
                        print(data.rKey)
                        print(data.startDate)
                        print(data.targetType)
                        print(data.title)
                        print("==========================================")
                        
                        
                        self.serverDatas.insert(data, at: 0)
                    }
                }
                self.viewInit()
            },  fail: { (response)->Void in
                print("response: \(response)")
            }
        )
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let defaultHeight: CGFloat = 65.0 //title label높이 + date label높이 + 여백
        let data = serverDatas[indexPath.row]
        var imgHeight: CGFloat = 0.0
        
        if data.imgUrl != "" {
            imgHeight = getURLImageHeight(urlString: data.imgUrl)
        } else {
            
        }
        let textHeight = heightForView(text: data.msg, font: UIFont.systemFont(ofSize: 15), width: 300)
        
        return defaultHeight + textHeight + imgHeight
    }
    ///////////////////////////////tableview/////////////////////////////////////////////
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return serverDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pushCell") as? PushTableCell
        
        if serverDatas.count > 0 {
//            print("indexPath.row: \(indexPath.row)")
            let data = serverDatas[indexPath.row]
            cell?.pushVC = self
            cell?.tag = indexPath.row
            
            cell?.setData(data)
            
            print("!!!!!!!!!!: \(prevCell?.tag)")
            //셀 선택할때 버튼, 백그라운드 처리
            cell?.tintColor = .black
            let colorView = UIView()
            colorView.backgroundColor = .clear
            UITableViewCell.appearance().selectedBackgroundView = colorView
        }
        
//        cell?.contentLabel.text = keywords[indexPath.row]
//        if keywordSwitch.isSelected {
//            cell?.contentLabel.textColor = .black
//        } else {
//            cell?.contentLabel.textColor = .lightGray
//        }
//        cell?.keywordVC = self
//        cell?.index_path = indexPath
        return cell!
    }
    
    //edit 버튼 눌렀을때 체크박스
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
//        unsafeBitCast(3, to: UITableViewCellEditingStyle.self)
        return UITableViewCellEditingStyle.init(rawValue: 3)!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "pushCell") as? PushTableCell
        if pushTableView.isEditing {
            cell?.isSelected = true
        } else {
            cell?.isSelected = false
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        print("@@@")
    }
    
    func test() {
        print("!!")
    }
    
    
    
    ///////////////////////////////tableview/////////////////////////////////////////////
    
    //셀 삭제 버튼
    @IBAction func deleteDataButton(_ sender: Any) {
//        notiTable.setEditing(true, animated: true)
//        pushTableView.setEditing(true, animated: true)
        if pushTableView.isEditing == false {
            editButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
            editButton.title = String.fontAwesomeIcon(name: .checkSquareO)
            closeButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
            closeButton.title = String.fontAwesomeIcon(name: .check)
            
            
            pushTableView.setEditing(true, animated: true)
            prevCell?.shareView.isHidden = true
        } else {
//            전체선택??
            

        }

    }
    
    //종료하기
    @IBAction func clickCloseButton(_ sender: Any) {
        if pushTableView.isEditing {
            editButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
            editButton.title = String.fontAwesomeIcon(name: .trash)
            closeButton.setTitleTextAttributes(fontAwesomeAttributes, for: .normal)
            closeButton.title = String.fontAwesomeIcon(name: .close)
            
            pushTableView.setEditing(false, animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

class PushTableCell: UITableViewCell {
    
    var pushVC: PushViewController?
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var shareView: UIView!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var imgView_Height_const: NSLayoutConstraint! //이미지뷰 높이
    @IBOutlet weak var shareButton_Width_const: NSLayoutConstraint! //공유하기 버튼 너비
    @IBOutlet weak var receiveButton_Width_const: NSLayoutConstraint! //혜택받기 버튼 너비
    @IBOutlet weak var detailButton_Width_const: NSLayoutConstraint! //자세히보기 버튼 너비
    
    //공유하기
    @IBAction func clickTest(_ sender: Any) {
        pushVC?.test()
        if pushVC?.pushTableView.isEditing == false {
            if self == pushVC?.prevCell {
                if shareView.isHidden {
                    shareView.isHidden = false
                } else {
                    shareView.isHidden = true
                }
            } else {
                pushVC?.prevCell?.shareView.isHidden = true
                shareView.isHidden = false
            }
            pushVC?.prevCell = self
        }
    }
    
    //혜택받기
    @IBAction func clickReceive(_ sender: UIButton) {
        receiveButton_Width_const.constant = 0
//        reloadInputViews()
    }
    
    //자세히보기
    @IBAction func clickDetail(_ sender: UIButton) {
        
    }
    
    //데이터 세팅
    func setData(_ data: Notification) {
        //이미지 있을때 체크해서 로드
        if data.imgUrl != "" {
            imgView_Height_const.constant = getURLImageHeight(urlString: data.imgUrl)
            imgView.af_setImage(withURL: URL(string: data.imgUrl)!)
        } else {
            imgView_Height_const.constant = 0.0
            imgView.image = UIImage()
        }
        //나머지 데이터 처리
        dateLabel.text = "\(data.startDate)"
        titleLabel.text = "\(data.title)"
        contentTextView.text = "\(data.msg)"
    }
    
    //테이블 재사용할때 초기화 메소드 여기서 초기값을 세팅해주면 된다.
    override func prepareForReuse() {
        super.prepareForReuse()
        shareView.isHidden = true
    }
}

class Notification {
    var changeDate: Date?
    var imgUrl: String = ""
    var linkUrl: String = ""
    var msg: String = ""
    var notiType: String = ""
    var rKey: String = ""
    var startDate: String = ""
    var targetType: String = ""
    var title: String = ""
    
    init(){}
}







