//
//  SideMenuViewController.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 9. 21..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit

class SideMenuViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.alpha = 0.5
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func clickCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
