//
//  MainViewController.swift
//  NewPowerapp
//
//  Created by SangDo on 2018. 9. 11..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//

import UIKit
import WebKit
import Alamofire
import AlamofireImage

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource, WKScriptMessageHandler {

    @IBOutlet weak var introImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collection_height_const: NSLayoutConstraint!
    @IBOutlet weak var menuCollectionLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var bottomSpaceView: UIView! //아이폰x용 하단 메뉴 꽉차게 해주는 뷰
    @IBOutlet weak var mainWebView: UIView!
    @IBOutlet weak var simpleStatusView: UIView!
    @IBOutlet weak var simpleStatusLabel: UILabel!
    @IBOutlet weak var sideView: UIView!
    @IBOutlet weak var sideBlackView: UIView!
    @IBOutlet weak var sideView_left_const: NSLayoutConstraint! //slideview 왼쪽 const값
    @IBOutlet weak var sideTableView: UITableView!
    @IBOutlet weak var sideTableView_left_const: NSLayoutConstraint! //사이드메뉴 전체 vs 4/3크기냐
    @IBOutlet weak var userInfoView: UIView!
    @IBOutlet weak var noLoginView: UIView!
    @IBOutlet weak var noLoginLockImage: UIImageView!
    @IBOutlet weak var noLoginTextLabel: UILabel!
    @IBOutlet weak var noLoginButton: UIButton!
    @IBOutlet weak var noLoginCloseButton: UIButton!
    @IBOutlet weak var yesLoginView: UIView!
    @IBOutlet weak var yesLoginBottomView: UIView!
    @IBOutlet weak var yesLoginCloseButton: UIButton!
    @IBOutlet weak var permissionView: UIView!
    
    @IBOutlet var backViews: [UIView]!
    @IBOutlet var lineViews: [UIView]!
    @IBOutlet var textlabels: [UILabel]!
    
    
//    let indicator = AlertIndicator()
    var isportrait = true
    var defaultUserAgent = "" ///기본 유저에이전트
    let cellWid: CGFloat = 40
    let cellHeaderHeight: CGFloat = 45
    var sideViewOpen = false
    var settingArr = [SideMenuCellEnum]() //설정 셀
    var reveryArr = [SideMenuCellEnum]() //간편기능 셀
    var pushiviteArr = [SideMenuCellEnum]() // 앱전용 알림/혜택 셀
    var versionArr = [SideMenuCellEnum]() // 버전 셀
    
    //하단메뉴리스트
    var collectionImageList = [(fileName: String, basicImage: String, tag: Int, url: String)]()
    
    //인트로화면 사라지는 타입
    var introType: IntroType = {
        guard !Config.shared.free_shop else { return .basic }
        //인트로시간이 0일 경우 인트로이미지 없음
        if Config.shared.use_intro_time == "0" {
            return .none
        }
        switch Config.shared.use_intro_effect {
        case "DISAPPEAR":
            return .basic
        case "SLIDE_UP":
            return .slideInFromBottom
        case "SLIDE_DOWN":
            return .slideOutFromTop
        case "SLIDE_RIGHT":
            return .swipeToRight
        case "SLIDE_LEFT":
            return .swipeToLeft
        case "REDUCE":
            return .gettingSmaller
        case "EXPAND":
            return .gettingBigger
        default:
            return .none
        }
    }()
    
    let url: URL = URL(string: "\(Config.shared.domain)/m")!
    var mainWKWebView: WKWebView!
    ///웹뷰의 현재 주소
    var webviewUrl: String = ""
    ///웹뷰가 이동할 주소
    fileprivate var strRequestURL: String = ""
    
    lazy var appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSlideType()
        
//        1.인트로 보여주기
        showIntroView()
        
//        2. 하단메뉴 넣어주기
        //맵을 사용하여 튜플을 원소로 하는 배열을 대입
        collectionImageList = TempVO.shared.botMenus.map {
            ($0.fileName, $0.basicImageName, $0.tag, $0.path) //튜플 리턴
        }
        
        UIView.animate(withDuration: 0, animations: {
            self.sideView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        })
        
//        3. 사이드 메뉴 셀 세팅해주기
        settingArr = [.notiSwitch, .editUserInfo]
        if Config.shared.slide_color != SLIDE_DEFAULT {
            settingArr.append(.keyword)
        }
        
        if Config.shared.slide_color == SLIDE_DEFAULT {
            reveryArr = [.delivery]
            pushiviteArr = [.notification, .invite]
        } else {
            reveryArr = [.delivery, .notification, .invite]
            if Config.shared.slide_color == SLIDE_BLACK {
                sideTableView.backgroundColor = UIColor(hexColorCode: "#1d1d1d")
                for backView in backViews {
                    backView.backgroundColor = UIColor(hexColorCode: "#1d1d1d")
                }
                for lineView in lineViews {
                    lineView.backgroundColor = UIColor(hexColorCode: "#ffffff")
                }
                for textlabel in textlabels {
                    textlabel.textColor = UIColor(hexColorCode: "#ffffff")
                }
                noLoginLockImage.image = UIImage(named: "ic_lock_light")
                noLoginButton.setBackgroundImage(UIImage(named: "btn_log_light"), for: .normal)
                noLoginCloseButton.setImage(UIImage(named: "ic_close_wh"), for: .normal)
            } else if Config.shared.slide_color == SLIDE_WHITE {
                sideTableView.backgroundColor = UIColor(hexColorCode: "#ffffff66")
                for backView in backViews {
                    backView.backgroundColor = UIColor(hexColorCode: "#ffffff66")
                }
                for lineView in lineViews {
                    lineView.backgroundColor = .black
                }
                for textlabel in textlabels {
                    textlabel.textColor = .black
                }
                
                
                noLoginView.backgroundColor = UIColor(hexColorCode: "#ffffff66")
                noLoginLockImage.image = UIImage(named: "ic_lock_dark")
                noLoginTextLabel.textColor = UIColor(hexColorCode: "#555555")
                noLoginButton.setBackgroundImage(UIImage(named: "btn_log_dark"), for: .normal)
                noLoginCloseButton.setImage(UIImage(named: "ic_close_bl"), for: .normal)
            } else {
                sideTableView.backgroundColor = .gray
                for backView in backViews {
                    backView.backgroundColor = .gray
                }
                for lineView in lineViews {
                    lineView.backgroundColor = .white
                }
                for textlabel in textlabels {
                    textlabel.textColor = .white
                }
                
                userInfoView.backgroundColor = UIColor(hexColorCode: "#000000E6")
                sideTableView.backgroundColor = UIColor(hexColorCode: "#000000E6")
                noLoginView.backgroundColor = UIColor(hexColorCode: "#000000E6")
                noLoginLockImage.image = UIImage(named: "ic_lock_light")
                noLoginTextLabel.textColor = UIColor(hexColorCode: "#ffffff")
                noLoginButton.setBackgroundImage(UIImage(named: "btn_log_light"), for: .normal)
                noLoginCloseButton.setImage(UIImage(named: "ic_close_wh"), for: .normal)
            }
        }
        versionArr = [.version]
        
        
//      4. statusView 세팅
        simpleStatusView.layer.cornerRadius = 15.0
        simpleStatusView.clipsToBounds = true
        simpleStatusView.alpha = 0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        for a in aa {
//            a.backgroundColor = .blue
//        }
        
//          1. 사이드 메뉴 크기
        if Config.shared.slide_type == "HALF" {
            sideTableView_left_const.constant = UIScreen.main.bounds.width / 4
        } else {
            sideTableView_left_const.constant = 0
        }
        sideTableView.reloadData()
        
//        2. 하단메뉴
        setPositionBottomMenu()
        
//        인트로 종료
        hideIntroView() {
            print("intro종료")
        }
        
//          3. 슬라이드 회원정보 노출
        if Config.shared.menu_user_info == "Y" {
            userInfoView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 140)
            //로그인체크 유무들어가야함
            //            self.noLoginView.isHidden = false
            checkSessionHandler(webView: mainWKWebView, { (result) -> Void in
                
                if !result {
                    self.yesLoginView.isHidden = true
                    self.noLoginView.isHidden = false
//                    self.yesLoginView.isHidden = false
//                    self.noLoginView.isHidden = true
                }
                else {
//                    self.yesLoginView.isHidden = true
//                    self.noLoginView.isHidden = false
                    self.yesLoginView.isHidden = false
                    self.noLoginView.isHidden = true
                }
            })
        } else {
            userInfoView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
        }
        
//        4. 하단 메뉴셀 노출세팅
        if Config.shared.bottom_display == "N" || Config.shared.bottom_display == "G" {
            collection_height_const.constant = 0
            mainWebView.frame = CGRect(x: 0, y: 0, width: mainWebView.frame.width, height: mainWebView.frame.height + 40)
            mainWKWebView.frame = CGRect(x: 0, y: 0, width: mainWebView.frame.width, height: mainWebView.frame.height)
        } else {
            collection_height_const.constant = 40
        }
        
//        5. 웹뷰 세팅
        let contentController = WKUserContentController()
        
        let progressFinishScript = "webkit.messageHandlers.powerapp_progress_finish.postMessage('Y')"
        let progressFinish: WKUserScript = WKUserScript(source: progressFinishScript,
                                                        injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        contentController.addUserScript(progressFinish)
        
        contentController.add(self, name: "powerapp_check_usim")
        contentController.add(self, name: "powerapp_login")
        contentController.add(self, name: "powerapp_logout")
        contentController.add(self, name: "powerappPushRkey")
        contentController.add(self, name: "powerapp_progress_finish")
        contentController.add(self, name: "basket_call")
        
        let config = WKWebViewConfiguration()
        config.userContentController = contentController
        config.allowsInlineMediaPlayback = true
        config.preferences.javaScriptCanOpenWindowsAutomatically = true
        
        
//        wkWebView = WKWebView(frame: UIScreen.main.bounds, configuration: config)
//        wkWebView.navigationDelegate = self
//        wkWebView.uiDelegate = self
        mainWKWebView = WKWebView(frame: createWKWebViewFrame(size: mainWebView.frame.size))
        mainWebView.addSubview(mainWKWebView)
        mainWKWebView.navigationDelegate = self
        mainWKWebView.uiDelegate = self
        let request = URLRequest(url: url)
        mainWKWebView.load(request)
        
        mainWKWebView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mainWKWebView.scrollView.scrollsToTop = true
        
//        mainView.insertSubview(wkWebView, belowSubview: progressView)
        //로딩바인데 언제쓰지
//        mainWKWebView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
        
        mainWKWebView.scrollView.isHidden = false
        
        let userAgent = userWebAgent()
        mainWKWebView.customUserAgent = userAgent
        
        let tempWebView = WKWebView(frame: CGRect.zero)
        view.insertSubview(tempWebView, belowSubview: mainWKWebView)
        tempWebView.loadHTMLString("<html> </ html>", baseURL: nil)
        tempWebView.evaluateJavaScript("navigator.userAgent") { (result, error) in
            if let agentString = result as? String {
                self.defaultUserAgent = agentString
                let userAgent = self.defaultUserAgent + self.powerappAddAgentString()
                self.mainWKWebView.customUserAgent = userAgent
                WKWebView().evaluateJavaScript(userAgent)
                UserDefaults.standard.register(defaults: ["UserAgent": userAgent])
            }
            
            if error != nil {
                DFT_TRACE_PRINT(output: error!.localizedDescription)
            }
            tempWebView.removeFromSuperview()
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            self.setPositionOrientate()
        }, completion: { (UIViewControllerTransitionCoordinatorContext) -> Void in
            DFT_TRACE_PRINT(output:"rotation completed")
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //인트로 이미지 보여주기
    func showIntroView() {
        defer {
            view.addSubview(introImageView)
        }
        
        if let url = URL(string: "\(Config.shared.introImage_url)") {
            introImageView.af_setImage(withURL: url)
        }
    }
    
    //인트로 이미지 숨기기
    func hideIntroView(completion:(() -> Void)? = nil) {
        var intro_time = Int(Config.shared.use_intro_time) ?? 0
        intro_time = 5
        let delay = DispatchTime.now() + .seconds(intro_time)
        
        DispatchQueue.main.asyncAfter(deadline: delay) {
            self.introRemoveAnimation(completion: completion)
        }
    }
    
    //인트로 이미지 사라지는 효과
    func introRemoveAnimation(completion:(() -> Void)? = nil) {
        let duration: TimeInterval = 1
        switch introType {
        case .basic:
            introImageView?.fadeOut(duration: duration) { _ in
                self.finishIntro(completion: completion)
            }
        case .slideOutFromTop:
            introImageView?.slideOutFromTop(duration: duration)
            finishIntro(completion: completion)
            
        case .slideInFromBottom:
            introImageView?.slideInFromBottom(duration: duration)
            finishIntro(completion: completion)
            
        case .swipeToRight:
            introImageView?.swipeToRight(duration: duration)
            finishIntro(completion: completion)
            
        case .swipeToLeft:
            introImageView?.swipeToLeft(duration: duration)
            finishIntro(completion: completion)
            
        case .gettingBigger:
            introImageView?.gettingBigger(duration: duration) { _ in
                self.finishIntro(completion: completion)
            }
        case .gettingSmaller:
            introImageView?.gettingSmaller(duration: duration) { _ in
                self.finishIntro(completion: completion)
            }
        default:
            finishIntro(completion: completion)
        }
    }
    
    //인트로 이미지 없애기
    func finishIntro(completion:(() -> Void)? = nil) {
        completion?()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.introImageView.removeFromSuperview()
        }
    }
    
    //하단 메뉴 포지션 세팅
    func setPositionBottomMenu() {
        let screenWid = collectionView.frame.size.width
        let menuNum = CGFloat(collectionImageList.count)
        
        menuCollectionLayout.minimumLineSpacing = 0
        menuCollectionLayout.itemSize = CGSize(width: cellWid, height: cellWid)
        
        if menuNum != 0 {
            if screenWid >= cellWid * menuNum {
                if menuNum > 1 {
                    let gap: CGFloat = (screenWid - cellWid * menuNum) / (menuNum - 1.0)
                    //                    위아래간격
                    //                    menuCollectionLayout.minimumLineSpacing = gap
                    menuCollectionLayout.minimumLineSpacing = gap
                } else {
                    menuCollectionLayout.sectionInset = UIEdgeInsetsMake(0, (screenWid-cellWid)/2, 0, 0)
                }
            } else {
                let cellWidEach: CGFloat = (cellWid * menuNum - screenWid) / menuNum
                menuCollectionLayout.itemSize = CGSize(width: cellWid-cellWidEach, height: cellWid)
            }
        }
    }
    
    fileprivate func setPositionOrientate() {
        let orient = UIApplication.shared.statusBarOrientation
        let screenWid = mainWebView.frame.width
        let screenHei = mainWebView.frame.height
        let dis = (screenWid-screenHei)/2
        
        mainWKWebView.frame = CGRect(x: 0, y: 0, width: screenWid, height: screenHei)
        
//        if introView != nil {
            switch orient {
            case .portrait:
                DFT_TRACE_PRINT(output: "Portrait Main")
//                if isFirstPortrait == true {
//                    isFirstPortrait = false
//
//                    if view.isiPhoneXScreen() {
//                        DFT_TRACE_PRINT(output: "~~~~~~~~~iPhoneX")
//
//                        var newWidth: CGFloat!
//
//                        //이미지가 없으면 리사이징 안함
//                        if introView?.image != nil {
//                            //세로 비율에 맞게 리사이징 - (이미지 넓이 * 디바이스 높이) / 이미지 높이
//                            newWidth = ((introView?.image?.size.width)! * screenHei) / (introView?.image?.size.height)!
//
//                            introView?.frame = CGRect(x: 0, y: 0, width: newWidth, height: screenHei)
//                            introView?.center.x = screenWid / 2
//                        }
//                    }
//                    else {
//                        introView?.frame = CGRect(x: 0, y: 0, width: screenWid, height: screenHei)
//                    }
//                }
//                else {
//                    introView?.frame = CGRect(x: -dis, y: dis, width: screenWid, height: screenHei)
//                }
//                introView?.transform = CGAffineTransform(rotationAngle: 0)
            case .landscapeLeft:
                DFT_TRACE_PRINT(output: "Portrait landscape Left")
//                introView?.frame = CGRect(x: dis, y: -dis, width: screenHei, height: screenWid)
//                introView?.transform = CGAffineTransform(rotationAngle: CGFloat(90 * Double.pi/180))
            case .landscapeRight:
                DFT_TRACE_PRINT(output: "Portrait landscape Right")
//                introView?.frame = CGRect(x: dis, y: -dis, width: screenHei, height: screenWid)
//                introView?.transform = CGAffineTransform(rotationAngle: CGFloat(-90 * Double.pi/180))
            default:
                break
            }
//        }
        
        if sideViewOpen == false {
            UIView.animate(withDuration: 0, animations: {
                self.sideView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
            })
        }
        
        
//        UIView.animate(withDuration: 0, animations: {
//            self.sideView.transform = CGAffineTransform.identity
//        }, completion: { Void in
//            self.sideView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
//        })
        
        setPositionBottomMenu()
        
        
        //회전시 팝업 이미지 리사이즈
//        popupVC?.resizePopView()
    }
    
    func powerappAddAgentString() -> String {
        let uKey: String = AES256Util.enc(with: "\(Config.shared.uuid)", key: TempVO.shared.mKey)
        let agent = ";FBPixel_\(Config.shared.adid);PowerApp_\(uKey);powerapp_build_20160615;makeshopapp"
        return agent
    }
    
    ///wk에서 브라우져에 유저에이전트를 넣는 시간이 느리기 때문에 직접 넣어 준다.
    func userWebAgent() -> String {
        var userAgent_str: String = ""
        
        if defaultUserAgent.isEmpty {
            let systemVersion : String = UIDevice.current.systemVersion
            let systemVersionUnderBar = systemVersion.replacingOccurrences(of: ".", with: "_")
            userAgent_str = "Mozilla/5.0 (iPhone; CPU iPhone OS \(systemVersionUnderBar) like Mac OS X) AppleWebKit/601.1.46 (KHTML, like Gecko) Mobile/13D15"
        } else {
            userAgent_str = defaultUserAgent
        }
        
        userAgent_str += powerappAddAgentString()
        
        return userAgent_str
    }

    //////////////////////////////////////////////////collcectionView////////////////////////////////////////////////////////////
    //셀 갯수
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionImageList.count
    }
    
    //셀 구성하기
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RowCell", for: indexPath) as! MainMenuCell
        let imgCell = collectionImageList[indexPath.row]
        cell.fileName = imgCell.fileName
        cell.basicImage = imgCell.basicImage
        cell.url = imgCell.url
        cell.tag = imgCell.tag
        
        if let url = URL(string: "\(imgCell.url)") {
            cell.menuImage.af_setImage(withURL: url)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MainMenuCell
        let selectTag = BottomMenu(rawValue: cell.tag) ?? .none
        
        switch selectTag {
        case .back:
//            원본
            historyBack()
            break
        case .forward:
            historyForward()
            break
        case .main:
            movePageURL(to: "\(Config.shared.domain)/m/")
        case .cateory:
            movePageURL(to: "\(Config.shared.domain)/m/category.html")
            break
        case .basket:
            movePageURL(to: "\(Config.shared.domain)/m/basket.html")
            break
        case .delivery:
            checkSessionHandler(webView: mainWKWebView, { (result) -> Void in
                if !result {
                    let customAllowedSet = CharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}").inverted
                    let url_encode = self.webviewUrl.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
                    self.movePageURL(to: "\(Config.shared.domain)/m/login.html?return_url=\(url_encode)")
                    self.showToast(message: "배송조회는 로그인 후 조회 가능합니다.")
                    return
                }
                else {
                    let vc = self.presentVC("DeliveryViewController")
                    self.present(vc, animated: true, completion: nil)
                }
            })
            break
        case .mypage:
            self.movePageURL(to: "\(Config.shared.domain)/m/mypage.html")
            break
        case .invite:
            checkSessionHandler(webView: mainWKWebView, { (result) -> Void in
                if !result {
                    let customAllowedSet = CharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}").inverted
                    let url_encode = self.webviewUrl.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
                    self.movePageURL(to: "\(Config.shared.domain)/m/login.html?return_url=\(url_encode)")
                    self.showToast(message: "친구초대는 로그인 후 초대 가능합니다.")
                    return
                }
                else {
                    //친구초대 팝업 어디다 만들어놨지
//                    let vc = self.presentVC("DeliveryViewController")
//                    self.present(vc, animated: true, completion: nil)
                }
            })
            break
        case .review:
//            checkSessionHandler({ (result) -> Void in
//                if !result {
//                    self.refreshPageAfterLogin(type: "review")
//                    self.showToast(message: "간편리뷰는 로그인 후 작성 가능합니다.")
//                    return
//                }
//                else {
//                    self.showReview()
//                }
//            })
            break
        case .push:
            let vc = self.presentVC("PushViewController")
            self.present(vc, animated: true, completion: nil)
            break
        case .sideMenu:
            UIView.animate(withDuration: 0.5, animations: {
                self.sideViewOpen = true
                self.sideView.transform = CGAffineTransform.identity
            }, completion: { Void in
                self.sideBlackView.isHidden = false
            })
//            sideMenuToggle()
            break
        case .reload:
            mainWKWebView.reloadFromOrigin()
            break
        case .scrap:
            showScrap()
            break
        case .hiddenTag_YG:
            //YG 히든태그 전용
//            break
            
        //새로운 메뉴가 추가될 경우 커스텀 메뉴 위에 추가
            break
        case .custom1:
            print("11")
            self.baroTalkBtnCheck(Config.shared.custom_1_link)
            break
        case .custom2:
            print("22")
            self.baroTalkBtnCheck(Config.shared.custom_2_link)
            break
        case .custom3:
            print("33")
            self.baroTalkBtnCheck(Config.shared.custom_3_link)
            break
        case .custom4:
            print("44")
            self.baroTalkBtnCheck(Config.shared.custom_4_link)
            break
        case .custom5:
            print("55")
            self.baroTalkBtnCheck(Config.shared.custom_5_link)
            break
        case .custom6:
            print("66")
            self.baroTalkBtnCheck(Config.shared.custom_6_link)
            break
        case .custom7:
            print("77")
            self.baroTalkBtnCheck(Config.shared.custom_7_link)
            break
        case .custom8:
            print("88")
            self.baroTalkBtnCheck(Config.shared.custom_8_link)
            break
        case .custom9:
            print("99")
            self.baroTalkBtnCheck(Config.shared.custom_9_link)
            break
        case .custom10:
            print("1010")
            self.baroTalkBtnCheck(Config.shared.custom_10_link)
            break
        default:
            break
        }
    }
    
    //뒤로가기
    func historyBack(_ isLogin: Bool = false) {
        if mainWKWebView.canGoBack {
            mainWKWebView.goBack()
        } else {
            let alert = AlertView(title: nil, message: ERROR_MOVE_PAGE, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //앞으로가기
    func historyForward() {
        if mainWKWebView.canGoForward {
            mainWKWebView.goForward()
        } else {
            let alert = AlertView(title: nil, message: ERROR_MOVE_PAGE, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func baroTalkBtnCheck(_ custom_link : String) {
        //하단 커스텀 버튼 바로톡인지 확인하여 바로톡일 경우 새로운 뷰에서 열리도록 함
        if custom_link.contains("baro.html") {
            //self.performSegueWithIdentifier("MBaro", sender: self)
//            openViewType = .baroTalk
//            webviewVC?.showBaroTalk()
        } else if custom_link.contains("barotalk_user_link.html") {
            //신바로톡
//            webviewVC?.moveNewBaroTalk()
        } else {
            movePageURL(to: "\(Config.shared.domain)/m/\(custom_link)")
        }
    }
    
    
    //////////////////////////////////////////////////collcectionView////////////////////////////////////////////////////////////
    
    
    //////////////////////////////////////////////////tableView////////////////////////////////////////////////////////////
    
    //사이드 메뉴 헤더 셀 갯수
    func numberOfSections(in tableView: UITableView) -> Int {
        return SideHeaderCell.count
    }
    
    //사이드 메뉴 헤더 셀 세팅
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as? HeaderTableCell
        headerCell?.headerTitle.text = SideHeaderCell[section]
        headerCell?.headerImage.image = UIImage(named: SideHeaderImage[section])

        //title label size
        let size = headerCell?.headerTitle.text?.size(withAttributes:[.font: UIFont.systemFont(ofSize:15.0)])
        let titleWidth = Int(ceil(Double((size?.width)!))) / 2
        //tableviewWidth
        let tableviewWidth = (tableView.bounds.width - sideTableView_left_const.constant)/2 - 34
        
        //프리미엄버전 메뉴 Title정렬 & 배경색
        if Config.shared.slide_color == SLIDE_DEFAULT {
            headerCell?.headerCell_left_const.constant = CGFloat(Int(-tableviewWidth) + titleWidth)
            headerCell?.backgroundColor = .white
        } else {
            if Config.shared.slide_T_align == SLIDE_T_CENTER {
                headerCell?.headerCell_left_const.constant = 0
            } else {
                //default 34 (34가 나온이유 여백 + 이미지크기 + 라벨과의여백)
                headerCell?.headerCell_left_const.constant = CGFloat(Int(-tableviewWidth) + titleWidth)
            }
            
            if Config.shared.slide_T_color == SLIDE_BLACK {
                headerCell?.backgroundColor = UIColor(hexColorCode: "#1d1d1d")
                headerCell?.headerTitle.textColor = UIColor(hexColorCode: "#ffffff")
            } else if Config.shared.slide_T_color == SLIDE_WHITE {
                headerCell?.backgroundColor = .white
                headerCell?.headerTitle.textColor = .black
            } else {
                headerCell?.backgroundColor = .gray
                headerCell?.headerTitle.textColor = .white
            }
        }
        

        switch section {
        case 0:
            //슬라이드에 회원정보가 노출될경우 X버튼을 없애준다.
            if Config.shared.menu_user_info == "Y" {
                headerCell?.headerButton.isHidden = true
            } else {
                headerCell?.headerButton.isHidden = false
            }
            
        case 1:
            print("!!1")
        case 2:
            print("!!2")
        case 3:
            print("!!3")
        default:
            return headerCell
        }
        return headerCell
    }
    
    //사이드 메뉴 헤더 셀 높이
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellHeaderHeight
    }
    
    //셀 갯수
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var cellNum = 1
        switch section {
        case 0:
            cellNum = settingArr.count
        case 1:
            cellNum = reveryArr.count
        case 2:
            if Config.shared.slide_color == SLIDE_DEFAULT {
                cellNum = pushiviteArr.count
            } else {
                cellNum = versionArr.count
            }
        case 3:
            cellNum = versionArr.count
        default:
            return 1
        }
        return cellNum
    }
    
    //셀 내용 세팅하기
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sideCell") as? SideTableCell
        let alarmCell = tableView.dequeueReusableCell(withIdentifier: "alarmCell") as? SideAlarmTableCell
        cell?.arrowImage.isHidden = false
        cell?.versionTextLabel.isHidden = true
        
        switch SideHeaderCell[indexPath.section] {
        case "설정":
            if indexPath.row == 0 {
                alarmCell?.alarmButton.tintColor = .red
            } else {
                cell?.contentTitle.text = settingArr[indexPath.row].rawValue
            }
        case "간편기능":
            cell?.contentTitle.text = reveryArr[indexPath.row].rawValue
        case "앱전용 알림/혜택":
            cell?.contentTitle.text = pushiviteArr[indexPath.row].rawValue
        case "버전":
            cell?.contentTitle.text = versionArr[indexPath.row].rawValue
            cell?.arrowImage.isHidden = true
            cell?.versionTextLabel.isHidden = false
        default:
            return cell!
        }
        
        cell?.selectionStyle = .none
        alarmCell?.selectionStyle = .none
        
        //프리미엄버전 메뉴 Title정렬 & 배경색
        if Config.shared.slide_color == SLIDE_DEFAULT {
            cell?.backgroundColor = UIColor(hexColorCode: "#ECECEC")
        } else {
            if Config.shared.slide_color == SLIDE_BLACK {
                cell?.backgroundColor = UIColor(hexColorCode: "#1d1d1d")
                alarmCell?.backgroundColor = UIColor(hexColorCode: "#1d1d1d")
                cell?.contentTitle.textColor = UIColor(hexColorCode: "#ffffff")
                if let texts = alarmCell?.alarmTextColors {
                    for text in texts {
                        text.textColor = UIColor(hexColorCode: "#ffffff")
                    }
                }
            } else if Config.shared.slide_color == SLIDE_WHITE {
                cell?.backgroundColor = .white
                alarmCell?.backgroundColor = .white
                cell?.contentTitle.textColor = .black
                if let texts = alarmCell?.alarmTextColors {
                    for text in texts {
                        text.textColor = .black
                    }
                }
            } else {
                cell?.backgroundColor = .gray
                alarmCell?.backgroundColor = .gray
                cell?.contentTitle.textColor = .white
                if let texts = alarmCell?.alarmTextColors {
                    for text in texts {
                        text.textColor = .white
                    }
                }
            }
        }
        
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                return alarmCell!
            } else {
                return cell!
            }
        default:
            return cell!
        }
    }
    
    //셀 클릭하기
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch SideHeaderCell[indexPath.section] {
        case "설정":
            switch settingArr[indexPath.row] {
            case .notiSwitch:
                print("notiSwitch")
            case .editUserInfo:
                // 회원 정보 수정 페이지로 이동
                print("!!")
//                mainVC!.movePageURL(to: "\(Config.shared.domain)/m/join_us.html")
            case .keyword :
                // 키워드 새창 띄우기
                let vc = presentVC("KeywordViewController")
                self.present(vc, animated: true, completion: nil)
            default:
                break
            }
        case "간편기능":
            //배송정보 확인하기
            switch reveryArr[indexPath.row] {
            case .delivery:
                checkSessionHandler(webView: mainWKWebView, { (result) -> Void in
                    if !result {
                        self.sideMenuClose()
                        let customAllowedSet = CharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}").inverted
                        let url_encode = self.webviewUrl.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
                        self.movePageURL(to: "\(Config.shared.domain)/m/login.html?return_url=\(url_encode)")
                        self.showToast(message: "배송조회는 로그인 후 조회 가능합니다.")
                        return
                    }
                    else {
                        let vc = self.presentVC("DeliveryViewController")
                        self.present(vc, animated: true, completion: nil)
                    }
                })
            case .notification:
                let vc = self.presentVC("PushViewController")
                self.present(vc, animated: true, completion: nil)
            case .invite:
                print("invite")
            default:
                break
            }
        case "앱전용 알림/혜택":
            print("AA")
        case "버전":
            print("AA")
        default:
            return
        }
    }
    
    //사이드 메뉴 닫기 버튼클릭
    @IBAction func clickSideMenuClose(_ sender: Any) {
        sideMenuClose()
    }
    //사이드 메뉴 하프일때 빈뷰 클릭
    @IBAction func clickSideMenuClose2(_ sender: Any) {
        sideMenuClose()
    }
    //사이드메뉴 회원정보 있을경우 & 로그인 안했을때 닫기 버튼
    @IBAction func clickNoLoginClose(_ sender: Any) {
        sideMenuClose()
    }
    //사이드메뉴 회원정보 있을경우 & 로그인했을때 닫기 버튼
    @IBAction func clickYesLoginClose(_ sender: Any) {
        sideMenuClose()
    }
    //오픈 소스버튼 클릭
    @IBAction func clickOpenSourceButton(_ sender: Any) {
        let vc = presentVC("OpenSourceViewController")
        self.present(vc, animated: true, completion: nil)
    }
    //사이드메뉴 닫기
    func sideMenuClose() {
        UIView.animate(withDuration: 0.5, animations: {
            self.sideBlackView.isHidden = true
            self.sideViewOpen = false
            self.sideView.transform = CGAffineTransform(translationX: self.view.frame.width, y: 0)
        })
    }
    //메인뷰에서 오른쪽으로 제스쳐 (전페이지로 이동)
    @IBAction func slideRightGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        if sender.state == .ended {
            //바로톡이 보이면 제스쳐 동작안함 추가해줘야함
            historyBack()
        }
    }
    //메인뷰에서 왼쪽으로 제스쳐 (사이드뷰나옴)
    @IBAction func slideLeftGesture(_ sender: UIScreenEdgePanGestureRecognizer) {
        UIView.animate(withDuration: 0.5, animations: {
            self.sideViewOpen = true
            self.sideView.transform = CGAffineTransform.identity
        }, completion: { Void in
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.sideBlackView.isHidden = false
            }
        })
    }
    //권한허용뷰 확인버튼
    @IBAction func clickHidePermission(_ sender: UIButton) {
        permissionView.fadeOut(duration: 0.5) { _ in
//            self.permissionView.isHidden = self.didShowPermissionView
            //권한허용이후 화면 회전 가능하게 설정
            self.appDelegate?.interfaceOrientationMask = .allButUpsideDown
        }
    }
    
    
    
    
    //////////////////////////////////////////////////tableView////////////////////////////////////////////////////////////
    
    /// 메인 웹뷰에 페이지 이동시켜주는 메소드
    func movePageURL(to url: String) {
//        if self.sideMenuConstraints.constant == 0 {
//            sideMenuToggle()
//        }
//        if TempVO.shared.param_keyword != "" {
//            if !url.contains("/order.html") {
//                if url.contains("?") {
//                    mainWKWebView.movePageURL(to: "\(url)&\(TempVO.shared.param_keyword)")
//                }
//                else {
//                    mainWKWebView.movePageURL(to: "\(url)?\(TempVO.shared.param_keyword)")
//                }
//            }
//            else {
//                TempVO.shared.param_keyword = ""
//            }
//        }
//        else {
//            mainWKWebView.movePageURL(to: url)
//        }
        
        if url.hasPrefix("http") || url.hasPrefix("powerapp://") {
            guard let main_url = encodedURL(string: url) else {
                let alert = AlertView(title: nil, message: ERROR_PAGE, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
                }, cancelCompletion: nil)
                self.present(alert, animated: true, completion: nil)
                return
            }
            var main_req = URLRequest(url: main_url)
            main_req.setValue("PowerApp", forHTTPHeaderField: "PowerApp")
            mainWKWebView.load(main_req)
        } else {
            let alert = AlertView(title: nil, message: ERROR_PAGE, okTitle: "확인", cancelTitle: "", okStyle: .default, okCompletion: {
            }, cancelCompletion: nil)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //로그인 안되있을경우 로그인페이지로 리다이렉트 시켜준다.
    func refreshPageAfterLogin() {
        let customAllowedSet = CharacterSet(charactersIn:"=\"#%/<>?@\\^`{|}").inverted
        let url_encode = webviewUrl.addingPercentEncoding(withAllowedCharacters: customAllowedSet)!
        movePageURL(to: "\(Config.shared.domain)/m/login.html?return_url=\(url_encode)")
    }
    
    //퍼가기 기능
    func showScrap() {
        //openViewType = "scrap"
        // let shopname = Config.shared.shopname //상점 이름 추가할 수 있음`
        let scrapURL = Config.shared.shopURL //초기값은 상점 URL로 설정하고
//        if !webviewVC!.webviewUrl.isEmpty { //웹뷰VC에서 받아온 값이 초기값이 아닐때
//            scrapURL = webviewVC!.webviewUrl //웹뷰의 URL을 가져온다.
//        }
        let objectsToShare = [scrapURL] //여기에 [ shopname, scrapURL]으로 변경하면 상점이름과 링크를 같이 보낼수 있다.
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
//            activityVC.popoverPresentationController?.sourceView = self.mainMenuCollectionView
            activityVC.popoverPresentationController?.sourceView = self.view
            activityVC.popoverPresentationController?.sourceRect = self.view.frame
        }
        present(activityVC, animated: true, completion: nil)
    }
    
    ///////////////////sidmenu
    
    
}

extension MainViewController {
    fileprivate func createWKWebViewFrame(size: CGSize) -> CGRect {
        let navigationHeight: CGFloat = 0
        let toolbarHeight: CGFloat = 0
        let height = size.height - navigationHeight - toolbarHeight
        return CGRect(x: 0, y: 0, width: size.width, height: height)
    }
    
    //토스트 형식의 알림창
    func showToast(message: String)  {
        simpleStatusView.layer.cornerRadius = 10.0
        simpleStatusLabel.text = message
        simpleStatusView?.alpha = 0
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.simpleStatusView?.alpha = 0.7
        }, completion: { (Bool) -> Void in
            DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                UIView.animate(withDuration: 0.3, animations: { () -> Void in
                    self.simpleStatusView!.alpha = 0
                })
            }
        })
    }
}

extension MainViewController: WKNavigationDelegate {
    //    순서: 11>22>11>66>33>44>11>55
    //탐색을 허용할지 취소할지 여부결정 (1)
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        var navi: WKNavigationActionPolicy = .allow
        
        webviewUrl = webView.url?.absoluteString ?? ""
        DFT_TRACE_PRINT(output:"webview에 요청된 URL =>\(webviewUrl)")
        
        let naviReq = navigationAction.request.url
        let reqUrl = NSString(string: naviReq?.absoluteString ?? "")
        
        if !String(reqUrl).contains("about:blank") {
            strRequestURL = reqUrl as String
            //파워앱 프레임워크 필요
//            TrackingAgent.trackingURL(webView, url: strRequestURL)
            
            if TempVO.shared.param_keyword != "" {
                strRequestURL = String(strRequestURL.components(separatedBy: TempVO.shared.param_keyword)[0])
                TempVO.shared.param_keyword = ""
            }
        }
        
        decisionHandler(navi)
    }
    
    //웹 콘텐츠가 로드되기 시작할때 (2) 파워앱에는 X
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    }
    
    //응답이 알려진 후 탐색을 허용할지 아니면 취소할지 결정 (3)
    func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
        print("webview33: \(webView.url?.absoluteString ?? "")")
        decisionHandler(.allow)
    }
    
    //웹보기에서 웹 콘텐츠를 받기 시작할때 (4)
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print("webview44 \(webView.url?.absoluteString ?? "")")
    }
    
    //완료될때 호출 (5)
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("webview55 \(webView.url?.absoluteString ?? "")")        
    }
    
    //웹보기가 서버 리다이렉션을 수신할때 파워앱에는 코드X
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
//        print("webview66: \(webView.url?.absoluteString ?? "")")
    }
    
    //네비게이션중에 에러발생
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("webview77")
    }
    
    //컨텐츠를 로드하는중에 오류
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("webview88")
        let nserror = error as NSError
        print("errorCode: \(nserror.code)")
    }
    
    //종료될때 호출
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print("webview99")
    }
    
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        print("webview?????")
    }
}

extension MainViewController: WKUIDelegate {
    //새 웹보기를 만듭니다.
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil {
            webView.load(navigationAction.request)
        }
        print("webview1010")
        return nil
    }
    
    // - MARK: 자바스크립트 Alert 관련 메소드
    
    //JavaScript 경고 패널을 표시합니다.
    func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: (@escaping () -> Void)) {
        print("webview1111")
        completionHandler()
    }
    
    //JavaScript 확인 ​​패널을 표시합니다.
    func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: (@escaping (Bool) -> Void)) {
        print("webview1212")
        completionHandler(true)
    }
    
    //JavaScript 텍스트 입력 패널을 표시합니다.
    func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
        print("webview1313")
        completionHandler(nil)
    }
}

//하단 메뉴 cell
class MainMenuCell: UICollectionViewCell {
    @IBOutlet weak var menuImage: UIImageView!
    var fileName: String? = nil
    var basicImage: String? = nil
    var url: String? = nil
}

//사이드 메뉴 cell
class SideTableCell: UITableViewCell {
    
    @IBOutlet weak var contentTitle: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var badgeImage: UIImageView!
    @IBOutlet weak var versionTextLabel: UILabel!
}

//사이드 메뉴 알림 cell
class SideAlarmTableCell: UITableViewCell {
    
    @IBOutlet weak var alarmTitle: UILabel!
    @IBOutlet weak var alarmContent: UILabel!
    @IBOutlet weak var alarmButton: UIButton!
    @IBOutlet var alarmTextColors: [UILabel]!
    
    
    //알림버튼 누르기
    @IBAction func clickAlarmButton(_ sender: Any) {
        if alarmButton.isSelected {
            alarmButton.setImage(UIImage(named: "toggle_off"), for: UIControlState())
            alarmButton.isSelected = false            
        } else {
            alarmButton.setImage(UIImage(named: "toggle_on"), for: .selected)
            alarmButton.isSelected = true
        }
    }
}

//사이드 메뉴 header cell
class HeaderTableCell: UITableViewCell {
    
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var headerButton: UIButton!
    @IBOutlet weak var headerCell_left_const: NSLayoutConstraint! //title 왼쪽정렬 vs 가운데 정렬
}










