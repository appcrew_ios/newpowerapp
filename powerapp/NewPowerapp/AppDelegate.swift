//
//  AppDelegate.swift
//  NewPowerapp
//
//  Created by  파워앱 on 2018. 8. 31..
//  Copyright © 2018년 koreacenter.com. All rights reserved.
//  TEST gggggg

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    //화면회전을 잠그고 고정할 목적의 플래그 변수
    var interfaceOrientationMask: UIInterfaceOrientationMask = .portrait

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let conf = Config.shared
        let temp = TempVO.shared
        
        if let shopSavedURL = Bundle.main.object(forInfoDictionaryKey: "ShopURL") as? String {
            conf.shopURL = shopSavedURL
        }
        if let shopSaveID = Bundle.main.object(forInfoDictionaryKey: "ShopID") as? String {
            conf.shopID = shopSaveID
        }
        conf.introImage_url = "\(conf.domain)/shopimages/\(conf.shopID)/powerapp_intro_android.png"
//        conf.uuid = getUUID()
        
        
        
        //메모리 부족으로 앱이 백그라운드에서 죽는 것을 방지하지 위함, 그래도 죽을 수 있음
        let sharedCache = URLCache.init(memoryCapacity: 4*1024*1024, diskCapacity: 32*1024*1024, diskPath: "nsurlcache")
        URLCache.shared = sharedCache
        
        //introviewController로 루트뷰 설정
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.makeKeyAndVisible()
        let introVC = IntroViewController()
        window.rootViewController = introVC
        self.window = window
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        print("background")
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // - MARK: 화면 회전제어
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return interfaceOrientationMask
//        return [.portrait, .landscape]
    }

//    func getUUID() -> String {
//        let severName = Bundle.main.bundleIdentifier!
//        if let retrieveuuid = SSKeychain.password(forService: severName, account: "user") {
//            print(retrieveuuid)
//            return retrieveuuid
//        }
//        else {
//            let uuidString: String = UIDevice.current.identifierForVendor!.uuidString
//            SSKeychain .setPassword(uuidString, forService: severName, account: "user")
//            print(uuidString)
//            return uuidString
//        }
//    }
    
    
    // MARK: - Core Data stack
    lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "Koreacenter.PowerApp" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "PowerApp", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        var coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("PowerApp.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        let options: [AnyHashable: Any] = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: options)
        } catch let error as NSError {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            DFT_TRACE_PRINT(output:"Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        } catch {
            DFT_TRACE_PRINT(output:"persistentStoreCoordinator error \(error.localizedDescription)")
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                DFT_TRACE_PRINT(output:"Unresolved error \(nserror.localizedDescription), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func readFromDocumentsFile(_ fileName:String) -> String {
        var file:String = ""
        if let dir: NSString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first as NSString? {
            let path = dir.appendingPathComponent(fileName)
            file = (try? String(contentsOfFile: path, encoding: .utf8)) ?? ""
        }
        return file
    }

}

